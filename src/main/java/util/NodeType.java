package util;

/**
 * Bieu dien cac node trong Function Detail Tree hoac Test Data Tree
 * Ex: GLOBAL, UUT, STUB, ...
 *
 *
 */
public enum NodeType {
    ROOT,
    GLOBAL,
    UUT,
    STUB,
    DONT_STUB,
    SBF,
    STATIC
}