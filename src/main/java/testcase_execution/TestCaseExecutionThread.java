package testcase_execution;

import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import parser.object.ICommonFunctionNode;
import testcase_manager.ITestCase;
import testcase_manager.TestCase;
import thread.AbstractVFPTask;
import util.VFPLogger;

public class TestCaseExecutionThread extends AbstractVFPTask<ITestCase> {
    private final static VFPLogger logger = VFPLogger.get(TestCaseExecutionThread.class);

    private final ITestCase testCase;
    // execute with google test, execute without google test, v.v.
    private int executionMode = TestcaseExecution.IN_EXECUTION_WITH_FRAMEWORK_TESTING_MODE;
    private boolean shouldShowReport = true;

    private EventHandler<WorkerStateEvent> preSucceededEvent;

    public TestCaseExecutionThread(ITestCase testCase) {
        this.testCase = testCase;
        this.testCase.setStatus(TestCase.STATUS_NA);
        setOnSucceeded(event -> {
            if (preSucceededEvent != null) {
                preSucceededEvent.handle(event);
            }
            // Generate test case data report
            if (shouldShowReport) {

            }
        });
    }

    public void setOnPreSucceededEvent(EventHandler<WorkerStateEvent> event) {
        this.preSucceededEvent = event;
    }

    @Override
    protected ITestCase call() {
        testCase.setStatus(TestCase.STATUS_EXECUTING);

        try {
            if (testCase instanceof TestCase)
                return callWithTestCase((TestCase) testCase);
        } catch (Exception e) {
            testCase.setStatus(TestCase.STATUS_FAILED);
            e.printStackTrace();
            logger.error("Can not execute test case " + testCase.getName());
        }

        return testCase;
    }

    private ITestCase callWithTestCase(TestCase testCase) throws Exception {
        ICommonFunctionNode function = testCase.getRootDataNode().getFunctionNode();

        // execute test case
        TestcaseExecution executor = new TestcaseExecution();
        executor.setFunction(function);
        if (testCase.getFunctionNode() == null)
            testCase.setFunctionNode(function);
        executor.setTestCase(testCase);
        executor.setMode(getExecutionMode());
        executor.execute();

        // export coverage of testcase to file
//        CoverageManager.exportCoveragesOfTestCaseToFile(testCase, Environment.getInstance().getTypeofCoverage());

        return testCase;
    }

    public int getExecutionMode() {
        return executionMode;
    }

    public void setExecutionMode(int executionMode) {
        this.executionMode = executionMode;
    }

    public void setShouldShowReport(boolean shouldShowReport) {
        this.shouldShowReport = shouldShowReport;
    }

    public boolean isShouldShowReport() {
        return shouldShowReport;
    }
}
