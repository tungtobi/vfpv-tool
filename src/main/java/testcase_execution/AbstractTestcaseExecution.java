package testcase_execution;

import auto_testcase_generation.cfg.testpath.ITestpathInCFG;
import auto_testcase_generation.maker.TestpathString_Marker;
import compiler.Compiler;
import compiler.Terminal;
import config.CommandConfig;
import environment.Environment;
import testcase_execution.testdriver.TestDriverGeneration;
import testcase_manager.ITestCase;
import testcase_manager.TestCase;
import util.CompilerUtils;
import util.Utils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public abstract class AbstractTestcaseExecution implements testcase_execution.ITestcaseExecution {
    protected TestDriverGeneration testDriverGen;
    private int mode = IN_EXECUTION_WITH_FRAMEWORK_TESTING_MODE; // by default
    private ITestCase testCase;

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public ITestCase getTestCase() {
        return testCase;
    }

    public void setTestCase(ITestCase testcase) {
        this.testCase = testcase;
    }

    public testcase_execution.IDriverGenMessage compileAndLink(CommandConfig customCommandConfig) throws IOException, InterruptedException {
        testcase_execution.DriverGenMessage genMessage = new testcase_execution.DriverGenMessage();

        StringBuilder compileMsg = new StringBuilder();

        Map<String, String> compilationCommands = customCommandConfig.getCompilationCommands();

        Compiler compiler = Environment.getInstance().getCompiler();

        // Create an executable file
        logger.debug("Compiling source code files");
        for (String filePath : compilationCommands.keySet()) {
            String compilationCommand = compilationCommands.get(filePath);

            logger.debug("Compile test driver:\nExecuting " + compilationCommand);

            String[] script = CompilerUtils.prepareForTerminal(compiler, compilationCommand);

            String response = new Terminal(script).get();

            compileMsg.append(response).append("\n");
        }

        genMessage.setCompileMessage(compileMsg.toString());

        String linkCommand = customCommandConfig.getLinkingCommand();
        logger.debug("Linking test driver: " + linkCommand);

        String[] linkScript = CompilerUtils.prepareForTerminal(compiler, linkCommand);
        String linkResponse = new Terminal(linkScript).get();
        genMessage.setLinkMessage(linkResponse);

        return genMessage;
    }

    public CommandConfig initializeCommandConfigToRunTestCase(ITestCase testCase) {
        /*
         * create the command file of the test case from the original command file
         */
        return testCase.generateCommands();
    }

    protected TestpathString_Marker readTestpathFromFile(ITestCase testCase) throws InterruptedException {
        TestpathString_Marker encodedTestpath = new TestpathString_Marker();

        int MAX_READ_FILE_NUMBER = 10;
        int countReadFile = 0;

        do {
            logger.debug("Finish. We are getting a execution path from hard disk");
            encodedTestpath.setEncodedTestpath(normalizeTestpathFromFile(
                    Utils.readFileContent(testCase.getTestPathFile())));

            if (encodedTestpath.getEncodedTestpath().length() == 0) {
                //initialization = "";
                Thread.sleep(10);
            }

            countReadFile++;
        } while (encodedTestpath.getEncodedTestpath().length() == 0 && countReadFile <= MAX_READ_FILE_NUMBER);

        return encodedTestpath;
    }

    protected String normalizeTestpathFromFile(String testpath) {
        testpath = testpath.replace("\r\n", ITestpathInCFG.SEPARATE_BETWEEN_NODES)
                .replace("\n\r", ITestpathInCFG.SEPARATE_BETWEEN_NODES)
                .replace("\n", ITestpathInCFG.SEPARATE_BETWEEN_NODES)
                .replace("\r", ITestpathInCFG.SEPARATE_BETWEEN_NODES);
        if (testpath.equals(ITestpathInCFG.SEPARATE_BETWEEN_NODES))
            testpath = "";
        return testpath;
    }

    protected TestpathString_Marker shortenTestpath(TestpathString_Marker encodedTestpath) {
        String[] executedStms = encodedTestpath.getEncodedTestpath().split(ITestpathInCFG.SEPARATE_BETWEEN_NODES);
        if (executedStms.length > 0) {
            int THRESHOLD = 200; // by default
            if (executedStms.length >= THRESHOLD) {
                logger.debug("Shorten test path to enhance code coverage computation speed: from "
                        + executedStms.length + " to " + THRESHOLD);
                StringBuilder tmp_shortenTp = new StringBuilder();

                for (int i = 0; i < THRESHOLD - 1; i++) {
                    tmp_shortenTp.append(executedStms[i]).append(ITestpathInCFG.SEPARATE_BETWEEN_NODES);
                }

                tmp_shortenTp.append(executedStms[THRESHOLD - 1]);
                encodedTestpath.setEncodedTestpath(tmp_shortenTp.toString());
            } else {
                logger.debug("No need for shortening test path because it is not too long");
            }
        }
        return encodedTestpath;
    }

    protected boolean analyzeTestpathFile(TestCase testCase) throws Exception {
        // Read hard disk until the test path is written into file completely
        TestpathString_Marker encodedTestpath = readTestpathFromFile(testCase);

        boolean success = true;

        // shorten test path if it is too long
        encodedTestpath = shortenTestpath(encodedTestpath);

        if (encodedTestpath.getEncodedTestpath().length() > 0) {
            // Only for logging
            success = computeCoverage(encodedTestpath, testCase);

            logger.debug("Retrieve the test path file "
                    + testCase.getTestPathFile() + " successfully.");
            logger.debug("Generate test paths for " + testCase.getName() + " sucessfully");

        } else {
            String msg = "The content of test path file is empty after execution";
            logger.debug(msg);
            if (/*getMode() == IN_EXECUTION_WITHOUT_GTEST_MODE
                    || */getMode() == IN_EXECUTION_WITH_FRAMEWORK_TESTING_MODE) {
                testCase.setStatus(TestCase.STATUS_FAILED);
            }
            success = false;
            throw new Exception(msg);
        }

        return success;
    }

    protected boolean computeCoverage(TestpathString_Marker encodedTestpath, TestCase testCase) {
//        // compute coverage
//        logger.debug("Compute coverage given the test path");
//
//        // coverage computation
//        ISourcecodeFileNode srcNode = Utils.getSourcecodeFile(testCase.getFunctionNode());
//        String tpContent = Utils.readFileContent(testCase.getTestPathFile());
//
//        SourcecodeCoverageComputation computer = new SourcecodeCoverageComputation();
//        try {
//            computer.setTestpathContent(tpContent);
//            computer.setConsideredSourcecodeNode(srcNode);
//            computer.setCoverage(Environment.getInstance().getTypeofCoverage());
//            computer.compute();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // highlighter
//        try {
//            SourcecodeHighlighterForCoverage highlighter = new SourcecodeHighlighterForCoverage();
//            highlighter.setSourcecode(srcNode.getAST().getRawSignature());
//            highlighter.setTestpathContent(tpContent);
//            highlighter.setSourcecodePath(srcNode.getAbsolutePath());
//            highlighter.setAllCFG(computer.getAllCFG());
//            highlighter.setTypeOfCoverage(computer.getCoverage());
//            highlighter.highlight();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // log to details tab of the testcase
//        if (getMode() == IN_AUTOMATED_TESTDATA_GENERATION_MODE) {
//            // log to details tab of the testcase
//            StringBuilder tp = new StringBuilder();
//            List<String> stms = encodedTestpath.getStandardTestpathByProperty(
//                    FunctionInstrumentationForStatementvsBranch_Markerv2.START_OFFSET_IN_FUNCTION);
//
//            if (stms.size() > 0) {
//                for (String stm : stms)
//                    tp.append(stm).append("=>");
//                tp = new StringBuilder(tp.substring(0, tp.length() - 2)); //
//                logger.debug("Done. Offsets of execution test path [length=" + stms.size() + "] = " + tp);
//                TCExecutionDetailLogger.logDetailOfTestCase(testCase, "Test case path: " + tp.toString());
//            } else {
//                logger.debug("Done. Offsets of execution test path [length=0]");
//                TCExecutionDetailLogger.logDetailOfTestCase(testCase, "No path");
//            }
//        }
//
//        return tpContent.contains(TestPathUtils.END_TAG);
        return true;
    }

    @Override
    public void initializeConfigurationOfTestcase(ITestCase testCase) {
        /*
         * Update test case
         */
        // test path
        testCase.setTestPathFileDefault();
        logger.debug("The test path file of test case " + testCase.getName() + ": " + testCase.getTestPathFile());

        // executable file
        testCase.setExecutableFileDefault();
        logger.debug("Executable file of test case " + testCase.getName() + ": " + testCase.getExecutableFile());

        // test case path
        testCase.setTestPathFileDefault();
        logger.debug("Path of the test case " + testCase.getName() + ": " + testCase.getTestPathFile());

        // source code file path
        testCase.setSourcecodeFileDefault();
        logger.debug("The source code file containing the test case " + testCase.getName() + ": " + testCase.getSourceCodeFile());

        // execution date and time
        testCase.setExecutionDateTime(LocalDateTime.now());

        testCase.setExecutedTime(-1);
    }

    protected String runExecutableFile(CommandConfig commandConfig) throws IOException, InterruptedException {
        String executableFilePath = commandConfig.getExecutablePath();

        Terminal terminal = new Terminal(executableFilePath);

        Process p = terminal.getProcess();
        p.waitFor(10, TimeUnit.SECONDS); // give it a chance to stop

        if (p.isAlive()) {
            p.destroy(); // tell the process to stop
            p.waitFor(10, TimeUnit.SECONDS); // give it a chance to stop
            p.destroyForcibly(); // tell the OS to kill the process
            p.waitFor();
        }

        testCase.setExecutedTime(terminal.getTime());

        return terminal.get();
    }

    public TestDriverGeneration getTestDriverGeneration() {
        return testDriverGen;
    }

    public void setTestDriverGeneration(TestDriverGeneration testDriverGeneration) {
        this.testDriverGen = testDriverGeneration;
    }
}
