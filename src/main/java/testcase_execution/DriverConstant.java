package testcase_execution;

public interface DriverConstant {

    String VFP_TEST_CASE_NAME = DriverConstant.TEST_NAME;

    String ASSERT_ENABLE = "ASSERT_ENABLE";

    String CALL_COUNTER = "VFP_fCall";

    String TEST_NAME = "VFP_test_case_name";

    String TEST_PATH_FILE_TAG = "{{INSERT_PATH_OF_TEST_PATH_HERE}}";

    String RUN_TEST = "VFP_run_test";

    String MARK = "VFP_mark";

    String EXEC_TRACE_FILE_TAG = "{{INSERT_PATH_OF_EXE_RESULT_HERE}}";

    String ASSERT_METHOD = "VFP_assert_method";

    String ASSERT_DOUBLE_METHOD = "VFP_assert_double_method";

    String ASSERT_PTR_METHOD = "VFP_assert_ptr_method";

    String ASSERT = "VFP_assert";

    String ASSERT_DOUBLE = "VFP_assert_double";

    String ASSERT_PTR = "VFP_assert_ptr";

    String ADDITIONAL_HEADERS_TAG = "/*{{INSERT_ADDITIONAL_HEADER_HERE}}*/";

    String INCLUDE_CLONE_TAG = "/*{{INSERT_CLONE_SOURCE_FILE_PATHS_HERE}}*/";

    String TEST_SCRIPTS_TAG = "/*{{INSERT_TEST_SCRIPTS_HERE}}*/";

    String COMPOUND_SET_UP = "/* Compound test case setup */";

    String COMPOUND_TEAR_DOWN = "/* Compound test case teardown */";

    String ADD_TESTS_TAG = "/*{{ADD_TESTS_STM}}*/";

    String DEFAULT_FUNCTIONS_TAG = "/*{{INSERT_DEFAULTS_FUNCTION_HERE}}*/";

}
