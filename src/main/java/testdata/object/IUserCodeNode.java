package testdata.object;

import user_code.objects.AbstractUserCode;

public interface IUserCodeNode {
    String DEFAULT_USER_CODE = "/* Write your own definition */";
    String VALUE_TAG = "<<value>>";

    /**
     * @return initial user code with only declaration
     */
    String generateInitialUserCode();

    void setUserCode(AbstractUserCode userCode);

    AbstractUserCode getUserCode();
}
