package testdata;

import auto_testcase_generation.testdatagen.RandomInputGeneration;
import boundary.DataSizeModel;
import boundary.PrimitiveBound;
import environment.Environment;
import parser.object.ICommonFunctionNode;
import parser.object.INode;
import project_init.ProjectClone;
import testcase_manager.IDataTestItem;
import testdata.gen.module.TreeExpander;
import testdata.object.*;
import testdata.object.stl.ListBaseDataNode;
import testdata.object.stl.STLArrayDataNode;
import testdata.object.stl.SmartPointerDataNode;
import testdata.object.stl.StdFunctionDataNode;
import util.ChooseRealTypeController;
import util.Utils;
import util.VFPLogger;
import util.VariableTypeUtils;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeTableCell;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static auto_testcase_generation.testdatagen.RandomInputGeneration.OPTION_VOID_POINTER_PRIMITIVE_TYPES;
import static auto_testcase_generation.testdatagen.RandomInputGeneration.OPTION_VOID_POINTER_STRUCTURE_TYPES;

public class InputCellHandler implements IInputCellHandler {

    public final static VFPLogger logger = VFPLogger.get(InputCellHandler.class);

    // store the template type to real type in template function, e.g, "T"->"int"
    // key: template type
    // value: real type
    private Map<String, String> realTypeMapping;
    private boolean inAutoGenMode = false;
    private IDataTestItem testCase;

    public InputCellHandler() {

    }

    @Override
    public void update(TreeTableCell<DataNode, String> cell, DataNode dataNode) {
        // first "if" to display "USER CODE" for parameters that use user code
        if (dataNode instanceof ValueDataNode && ((ValueDataNode) dataNode).isUseUserCode()) {
            cell.setEditable(true);
            cell.setText(((ValueDataNode) dataNode).getUserCodeDisplayText());
        } else if (dataNode instanceof NormalNumberDataNode) {
            // int
            cell.setEditable(true);
            cell.setText(((NormalNumberDataNode) dataNode).getValue());

        } else if (dataNode instanceof NormalCharacterDataNode) {
            // char
            cell.setEditable(true);
            cell.setText(((NormalCharacterDataNode) dataNode).getValue());

        } else if (dataNode instanceof NormalStringDataNode) {
            // char
            cell.setEditable(true);
            cell.setText("<<Size: " + dataNode.getChildren().size() + ">>");


        } else if (dataNode instanceof EnumDataNode) {
            // enum
            cell.setText("Select value");
            if (((EnumDataNode) dataNode).isSetValue()) {
                cell.setText(((EnumDataNode) dataNode).getValue());
            }

        } else if (dataNode instanceof UnionDataNode) {
            // union
            cell.setText("Select attribute");
            if (!dataNode.getChildren().isEmpty()) {
                cell.setText(dataNode.getChildren().get(0).getName());
            }

        } else if (dataNode instanceof SubClassDataNode) {
            // subclass
            cell.setEditable(true);
            SubClassDataNode subClassDataNode = (SubClassDataNode) dataNode;
            cell.setText("Select constructor");
            if (subClassDataNode.getSelectedConstructor() != null) {
                // Hiển thị tên constuctor class
                cell.setText(subClassDataNode.getSelectedConstructor().getName());
            }

        } else if (dataNode instanceof ClassDataNode) {
            // class
            cell.setEditable(true);
            ClassDataNode classDataNode = (ClassDataNode) dataNode;
            cell.setText("Select real class");
            if (classDataNode.getSubClass() != null) {
                // Hiển thị tên class
                cell.setText(classDataNode.getSubClass().getRawType());
            }

        } else if (dataNode instanceof OneDimensionDataNode) {
            // array
            OneDimensionDataNode arrayNode = (OneDimensionDataNode) dataNode;
            if (arrayNode.isFixedSize()) {
                cell.setText(toSize(((OneDimensionDataNode) dataNode).getSize() + ""));
            } else {
                cell.setEditable(true);
                cell.setText("<<Define size>>");
                if (arrayNode.isSetSize()) {
                    cell.setText(toSize(arrayNode.getSize() + ""));
                }
            }

        } else if (dataNode instanceof PointerDataNode) {
            // con trỏ coi như array
            cell.setEditable(true);
            PointerDataNode arrayNode = (PointerDataNode) dataNode;
            cell.setText("<<Define size>>");
            if (arrayNode.isSetSize()) {
                cell.setText(toSize(arrayNode.getAllocatedSize() + ""));
            }

        } else if (dataNode instanceof MultipleDimensionDataNode) {
            // mảng 2 chiều của int, char
            cell.setEditable(true);
            MultipleDimensionDataNode arrayNode = (MultipleDimensionDataNode) dataNode;
            cell.setText("<<Define size>>");
            if (arrayNode.isSetSize()) {
                StringBuilder sizesInString = new StringBuilder();
                int lastIdx = arrayNode.getSizes().length - 1;
                for (int i = 0; i < lastIdx; i++)
                    sizesInString.append(arrayNode.getSizes()[i]).append(" x ");
                sizesInString.append(arrayNode.getSizes()[lastIdx]);

                cell.setText(toSize(sizesInString + ""));
            }

        } else if (dataNode instanceof ListBaseDataNode) {
            ListBaseDataNode vectorNode = (ListBaseDataNode) dataNode;
            if (dataNode instanceof STLArrayDataNode)
                cell.setEditable(false);
            else
                cell.setEditable(true);
            cell.setText("<<Define size>>");
            if (vectorNode.isSetSize()) {
                cell.setText(toSize(vectorNode.getSize() + ""));
            }

        } else if (dataNode instanceof TemplateSubprogramDataNode) {
            cell.setEditable(true);
            cell.setText("Change template");

        } else if (dataNode instanceof SmartPointerDataNode) {
            cell.setEditable(true);
            cell.setText("Choose constructor");

        } else if (dataNode instanceof FunctionPointerDataNode) {
            INode selected = ((FunctionPointerDataNode) dataNode).getSelectedFunction();
            boolean isDefault = ((FunctionPointerDataNode) dataNode).isUseDefault();
            if (isDefault) {
                cell.setText("<<Default>>");
            } else if (selected != null) {
                cell.setText(selected.getName());
            } else {
                cell.setText("<<Choose reference>>");
            }
        } else if (dataNode instanceof VoidPointerDataNode) {
            cell.setEditable(false);
//            String shorten = ((VoidPointerDataNode) dataNode).getUserCode();
//            shorten = shorten.replace("\n", "↵");
//            cell.setText(shorten);
            String realType = ((VoidPointerDataNode) dataNode).getReferenceType();
            if (realType != null) {
                cell.setText(realType);
            } else {
                cell.setText("Choose real type");
            }

        } else if (dataNode instanceof OtherUnresolvedDataNode) {
            cell.setEditable(false);
            String shorten = ((OtherUnresolvedDataNode) dataNode).getUserCode().getContent();
            shorten = shorten.replace("\n", "↵");
            cell.setText(shorten);

        } else if (dataNode instanceof NullPointerDataNode) {
            cell.setText(NullPointerDataNode.NULL_PTR);
            cell.setGraphic(null);

        } else {
            cell.setText(null);
            cell.setGraphic(null);
        }
    }

    @Override
    public void commitEdit(ValueDataNode dataNode, String newValue) throws Exception {
        boolean is_realCommit = true;

        if (dataNode instanceof NormalNumberDataNode) {
            String type = dataNode.getRealType();
            type = VariableTypeUtils.removeRedundantKeyword(type);
            if (VariableTypeUtils.isNumFloat(type)) {
                try {
                    double value = Double.parseDouble(newValue);
                    PrimitiveBound bound = Environment.getBoundOfDataTypes().getBounds().get(type);
                    if (bound != null) {
                        if (value >= Double.parseDouble(bound.getLower())
                                && value <= Double.parseDouble(bound.getUpper())) {
                            if (type.equals("bool")) {
                                if (newValue.toLowerCase().equals("true") || newValue.toLowerCase().equals("false"))
                                    ((NormalNumberDataNode) dataNode).setValue(newValue);
                                else if (newValue.equals("1"))
                                    ((NormalNumberDataNode) dataNode).setValue("true");
                                else if (newValue.equals("0"))
                                    ((NormalNumberDataNode) dataNode).setValue("false");
                            } else
                                ((NormalNumberDataNode) dataNode).setValue(String.valueOf(value));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("Do not handle when committing " + dataNode.getClass());
                }
            } else {
                try {
                    long value = Long.parseLong(newValue);
                    if (VariableTypeUtils.isStdInt(type) || VariableTypeUtils.isTimet(type) || VariableTypeUtils.isSizet(type)) {
                        ((NormalNumberDataNode) dataNode).setValue(value + "");
                    } else {

                        PrimitiveBound bound = Environment.getBoundOfDataTypes().getBounds().get(type);
                        if (bound != null) {
                            if (value >= Long.parseLong(bound.getLower())
                                    && value <= Long.parseLong(bound.getUpper())) {
                                if (type.equals("bool")) {
                                    if (newValue.toLowerCase().equals("true") || newValue.toLowerCase().equals("false"))
                                        ((NormalNumberDataNode) dataNode).setValue(newValue);
                                    else if (newValue.equals("1"))
                                        ((NormalNumberDataNode) dataNode).setValue("true");
                                    else if (newValue.equals("0"))
                                        ((NormalNumberDataNode) dataNode).setValue("false");
                                } else
                                    ((NormalNumberDataNode) dataNode).setValue(value + "");
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (dataNode instanceof NormalCharacterDataNode) {
            // CASE: Type character
            if (newValue.startsWith(NormalCharacterDataNode.VISIBLE_CHARACTER_PREFIX)
                    && newValue.endsWith(NormalCharacterDataNode.VISIBLE_CHARACTER_PREFIX)) {
                String character = newValue.substring(1, newValue.length() - 1);

                String ascii = NormalCharacterDataNode.getCharacterToACSIIMapping().get(character);
                if (ascii != null)
                    ((NormalCharacterDataNode) dataNode).setValue(ascii + "");
                else {
                    logger.error("Do not handle when the length of text > 1 for character parameter");
                }

            } else {
                try {
                    // CASE: Type ascii
                    Long v = Long.parseLong(newValue);
                    DataSizeModel dataSizeModel = Environment.getBoundOfDataTypes().getBounds();
                    PrimitiveBound bound = dataSizeModel.get(dataNode.getRawType());
                    if (bound == null)
                        bound = dataSizeModel.get(dataNode.getRawType().replace("std::", "").trim());

                    if (bound == null) {

                    } else if (v <= bound.getUpperAsLong() && v >= bound.getLowerAsLong()) {
                        ((NormalCharacterDataNode) dataNode).setValue(newValue);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("Do not handle when the length of text > 1 for character parameter");
                }
            }
        } else if (dataNode instanceof NormalStringDataNode) {
            try {
                long lengthOfString = Long.parseLong(newValue);

                if (lengthOfString < 0)
                    throw new Exception();

                ((NormalStringDataNode) dataNode).setAllocatedSize(lengthOfString);
                TreeExpander expander = new TreeExpander();
                expander.setRealTypeMapping(this.realTypeMapping);
                expander.expandTree(dataNode);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (dataNode instanceof EnumDataNode) {
            // enum oke
            ((EnumDataNode) dataNode).setValue(newValue);
            ((EnumDataNode) dataNode).setValueIsSet(true);

        } else if (dataNode instanceof UnionDataNode) {
            // union oke
            // expand tree với thuộc tính được chọn ở combobox
            (new TreeExpander()).expandStructureNodeOnDataTree(dataNode, newValue);

        } else if (dataNode instanceof StructDataNode) {
            (new TreeExpander()).expandStructureNodeOnDataTree(dataNode, newValue);

        } else if (dataNode instanceof SubClassDataNode) {
            // subclass (cũng là class) oke
            ((SubClassDataNode) dataNode).chooseConstructor(newValue);
            (new TreeExpander()).expandTree(dataNode);

        } else if (dataNode instanceof ClassDataNode) {
            // class oke
            ((ClassDataNode) dataNode).setSubClass(newValue);

        } else if (dataNode instanceof OneDimensionDataNode) {
            //array cua normal data. oke
            int size = Integer.parseInt(newValue);
            OneDimensionDataNode currentNode = (OneDimensionDataNode) dataNode;
            currentNode.setSize(size);
            currentNode.setSizeIsSet(true);

            TreeExpander expander = new TreeExpander();
            expander.setRealTypeMapping(this.realTypeMapping);
            expander.expandTree(dataNode);

        } else if (dataNode instanceof PointerDataNode) {
            // con trỏ coi như array
            int size = Integer.parseInt(newValue);
            PointerDataNode currentNode = (PointerDataNode) dataNode;
            currentNode.setAllocatedSize(size);
            // tmp
            currentNode.setSizeIsSet(true);

            TreeExpander expander = new TreeExpander();
            expander.setRealTypeMapping(this.realTypeMapping);
            expander.expandTree(dataNode);

        } else if (dataNode instanceof MultipleDimensionDataNode) {
            int sizeA = Integer.parseInt(newValue);
            MultipleDimensionDataNode currentNode = (MultipleDimensionDataNode) dataNode;
            currentNode.setSizeOfDimension(0, sizeA);
            currentNode.setSizeIsSet(true);

            TreeExpander expander = new TreeExpander();
            expander.expandTree(dataNode);

        } else if (dataNode instanceof TemplateSubprogramDataNode) {
            dataNode.getChildren().clear();
            ((TemplateSubprogramDataNode) dataNode).setRealFunctionNode(newValue);
//            ((TemplateDataNode) dataNode).generateArgumentsAndReturnVariable();

        } else if (dataNode instanceof SmartPointerDataNode) {
            dataNode.getChildren().clear();
            ((SmartPointerDataNode) dataNode).chooseConstructor(newValue);
            (new TreeExpander()).expandTree(dataNode);

        } else if (dataNode instanceof ListBaseDataNode && !(dataNode instanceof STLArrayDataNode)) {
            //array cua normal data. oke
            int size = Integer.parseInt(newValue);
            ListBaseDataNode currentNode = (ListBaseDataNode) dataNode;
            currentNode.setSize(size);
            currentNode.setSizeIsSet(true);

            TreeExpander expander = new TreeExpander();
            expander.expandTree(dataNode);

        } else if (dataNode instanceof FunctionPointerDataNode) {
            if (newValue.equals(FunctionPointerDataNode.DEFAULT)) {
                ((FunctionPointerDataNode) dataNode).useDefault();
            } else {
                FunctionPointerDataNode fpDataNode = (FunctionPointerDataNode) dataNode;
                fpDataNode.getPossibleFunctions()
                        .stream()
                        .filter(f -> f.getName().equals(newValue))
                        .findFirst()
                        .ifPresent(f -> {
                            if (f instanceof ICommonFunctionNode) {
                                commitSelectedReference(fpDataNode, (ICommonFunctionNode) f);
                            }
                        });
            }
        } else if (dataNode instanceof OtherUnresolvedDataNode) {
            logger.debug("OtherUnresolvedDataNode");
//            ((OtherUnresolvedDataNode) dataNode).setUserCode(newValue);

        } else if (dataNode instanceof VoidPointerDataNode) {
            commitVoidPtrInputMethod(dataNode, newValue);
            if (((VoidPointerDataNode) dataNode).getReferenceType() == null) {
                is_realCommit = false;
            }

        } else if (dataNode instanceof StdFunctionDataNode) {
            if (newValue.isEmpty()) {
                ((StdFunctionDataNode) dataNode).setBody(newValue);
            } else {
                ((StdFunctionDataNode) dataNode).setBody(String.format("return %s;", newValue));
            }

        } else
            logger.error("Do not support to enter data for " + dataNode.getClass());

        if (is_realCommit) {
            dataNode.setUseUserCode(false);
        }
    }

    private void commitVoidPtrInputMethod(DataNode dataNode, String inputMethod) {
        if (isInAutoGenMode()) {
            ChooseRealTypeController controller = new ChooseRealTypeController();
            /**
             * Step 1
             */
            final String DELIMITER_BETWEEN_ATTRIBUTE = ",";
            final String DELIMITER_BETWEEN_KEY_AND_VALUE = "=";
            String[] elements = inputMethod.split(DELIMITER_BETWEEN_ATTRIBUTE);
            Map<String, String> elementMap = new HashMap<>();
            for (String element : elements)
                elementMap.put(
                        element.split(DELIMITER_BETWEEN_KEY_AND_VALUE)[0],
                        element.split(DELIMITER_BETWEEN_KEY_AND_VALUE)[1]
                );

            String category = elementMap.get(RandomInputGeneration.VOID_POINTER____SELECTED_CATEGORY);
            controller.setTestCase(testCase);
            controller.loadContent(category);

            if (category != null
                    && (category.equals(OPTION_VOID_POINTER_PRIMITIVE_TYPES)
                    || category.equals(OPTION_VOID_POINTER_STRUCTURE_TYPES))) {
                String coreType = elementMap.get(RandomInputGeneration.VOID_POINTER____SELECTED_CORE_TYPE);
                ObservableList<INode> possibleNodes = controller.getLvRealTypes().getItems();

                int level = Integer.parseInt(elementMap.get(RandomInputGeneration.VOID_POINTER____POINTER_LEVEL));
                for (INode possibleNode : possibleNodes)
                    if (possibleNode.getName().equals(coreType)) {
                        controller.chooseANode(possibleNode, testCase, (ValueDataNode) dataNode, level);
                        break;
                    }
            }
        } else {
            if (inputMethod.equals(OPTION_VOID_POINTER_PRIMITIVE_TYPES)
                    || inputMethod.equals(OPTION_VOID_POINTER_STRUCTURE_TYPES)) {
                ChooseRealTypeController controller = ChooseRealTypeController.getInstance(dataNode);
                if (controller != null && controller.getStage() != null) {
                    controller.setTestCase(testCase);
                    controller.loadContent(inputMethod);
                    Stage window = controller.getStage();
                    window.setResizable(false);
                    window.initModality(Modality.WINDOW_MODAL);
                    window.showAndWait();
                }
            }
        }
    }

    public void commitSelectedReference(FunctionPointerDataNode fpDataNode, ICommonFunctionNode f) {
        fpDataNode.setSelectedFunction(f);
        if (testCase != null) {
            //TODO: relative
            String filePath = Utils.getSourcecodeFile(f).getAbsolutePath();
            String cloneFilePath = ProjectClone.getClonedFilePath(filePath);
            if (!new File(cloneFilePath).exists())
                cloneFilePath = filePath;
            testCase.putOrUpdateDataNodeIncludes(fpDataNode, cloneFilePath);
        }
    }

    public void setTestCase(IDataTestItem testCase) {
        this.testCase = testCase;
    }

    public IDataTestItem getTestCase() {
        return testCase;
    }

    public void setRealTypeMapping(Map<String, String> realTypeMapping) {
        this.realTypeMapping = realTypeMapping;
    }

    private String toSize(String size) {
        if (size.equals("0") || size.equals("-1"))
            return "<<Size: NULL>>";
        else
            return "<<Size: " + size + ">>";
    }

    public void setInAutoGenMode(boolean inAutoGenMode) {
        this.inAutoGenMode = inAutoGenMode;
    }

    public boolean isInAutoGenMode() {
        return inAutoGenMode;
    }
}
