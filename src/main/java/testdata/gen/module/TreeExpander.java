package testdata.gen.module;

import parser.object.*;
import testdata.object.*;
import util.VFPLogger;

/**
 *
 */
public class TreeExpander extends AbstractDataTreeExpander {
    final static VFPLogger logger = VFPLogger.get(TreeExpander.class);

//    private ICommonFunctionNode functionNode;

    public TreeExpander() {
//        super(false);
    }

//    public TreeExpander(boolean skipTypeResolver) {
//        super(skipTypeResolver);
//    }

    public void expandStructureNodeOnDataTree(ValueDataNode node, String name) throws Exception {
        node.getChildren().clear();

        VariableNode vParent = node.getCorrespondingVar();
        INode correspondingNode = vParent.resolveCoreType();

        if (correspondingNode instanceof StructureNode) {
            StructureNode childClass = (StructureNode) correspondingNode;
            for (IVariableNode n : childClass.getPublicAttributes()) {
                if (n.getName().contains(name))
                    generateStructureItem((VariableNode) n, vParent + "." + name, node);
            }
        }else{
            logger.error("Do not handle the case " + correspondingNode.getClass());
        }
    }

//	public ICommonFunctionNode getFunctionNode() {
//		return functionNode;
//	}

//	public void setFunctionNode(ICommonFunctionNode functionNode) {
//		this.functionNode = functionNode;
//	}
}
