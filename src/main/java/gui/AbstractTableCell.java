package gui;

import javafx.scene.control.TextField;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTablePosition;
import testcase_manager.IDataTestItem;
import testdata.InputCellHandler;
import testdata.object.*;
import testdata.object.stl.ListBaseDataNode;
import testdata.object.stl.STLArrayDataNode;
import util.VFPLogger;

public abstract class AbstractTableCell extends TreeTableCell<DataNode, String> {
    
    private final static VFPLogger logger = VFPLogger.get(AbstractTableCell.class);

    private IDataTestItem testCase;
    protected InputCellHandler inputCellHandler;

    protected boolean escapePressed = false;
    protected TreeTablePosition<DataNode, ?> tablePos = null;

    public enum CellType {
        INPUT,
        EXPECTED,
    }

    AbstractTableCell(IDataTestItem testCase) {
        this.testCase = testCase;
        inputCellHandler = new InputCellHandler();
        inputCellHandler.setTestCase(testCase);
    }

    public IDataTestItem getTestCase() {
        return testCase;
    }

    private String getValueForTextField(DataNode dataNode) {
        if (dataNode instanceof NormalDataNode) {
            return ((NormalDataNode) dataNode).getValue();

        } else if (dataNode instanceof OneDimensionDataNode && !((OneDimensionDataNode) dataNode).isFixedSize()) {
            return String.valueOf(((OneDimensionDataNode) dataNode).getSize());

        } else if (dataNode instanceof PointerDataNode) {
            // con trỏ coi như array
            PointerDataNode arrayNode = (PointerDataNode) dataNode;
            return String.valueOf(((PointerDataNode) dataNode).getAllocatedSize());

        } else if (dataNode instanceof MultipleDimensionDataNode) {
            // mảng 2 chiều của int, char
            MultipleDimensionDataNode arrayNode = (MultipleDimensionDataNode) dataNode;
            if (arrayNode.isSetSize()) {
                StringBuilder sizesInString = new StringBuilder();
                int lastIdx = arrayNode.getSizes().length - 1;
                for (int i = 0; i < lastIdx; i++)
                    sizesInString.append(arrayNode.getSizes()[i]).append(" x ");
                sizesInString.append(arrayNode.getSizes()[lastIdx]);
                return sizesInString.toString();
            }

        } else if (dataNode instanceof ListBaseDataNode && !(dataNode instanceof STLArrayDataNode)) {
            ListBaseDataNode vectorNode = (ListBaseDataNode) dataNode;
            if (vectorNode.isSetSize()) {
                return String.valueOf(vectorNode.getSize());
            }
        }

        return null;
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
    }

    public static final String OPTION_VOID_POINTER_PRIMITIVE_TYPES = "Primitive Types";
    public static final String OPTION_VOID_POINTER_STRUCTURE_TYPES = "Structure Types";
    public static final String OPTION_VOID_POINTER_USER_CODE = "User Code";
}