package gui;

import auto_testcase_generation.track.MetricsTimer;
import coverage.CoverageDataObject;
import environment.Environment;
import javafx.application.Platform;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import parser.dependency.FunctionCallDependency;
import parser.object.IFunctionNode;
import parser.object.INode;
import parser.object.IVariableNode;
import parser.object.StructureNode;
import search.Search;
import search.condition.AbstractFunctionNodeCondition;
import search.condition.FunctionNodeCondition;
import testcase_manager.TestCase;
import thread.AbstractVFPTask;
import thread.VFPThread;
import thread.task.BuildEnvironmentTask;
import thread.task.GenerateTestdataTask;
import thread.task.PostProcessTask;
import util.VariableTypeUtils;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class BaseController implements Initializable {

    private static BaseController instance;


    @FXML
    private Label lMem;

    @FXML
    private HBox pLoading;
    @FXML
    private Text tCoverage;
    @FXML
    private ListView<INode> lvFiles;
    @FXML
    private TextField tfPath;
    @FXML
    private ListView<IFunctionNode> lvFunctions;
    @FXML
    private ListView<TestCase> lvTestCases;

    private CoverageDataObject coverage;

    @FXML
    public void generate(ActionEvent actionEvent) {
        showLoading();
        IFunctionNode functionNode = lvFunctions.getSelectionModel().getSelectedItem();
        generate(functionNode);
    }

    private void generate(IFunctionNode functionNode) {
        GenerateTestdataTask task = new GenerateTestdataTask();
        task.setFunction(functionNode);
        VFPThread thread = new VFPThread(task);
        thread.start();
    }

    @FXML
    public void browser(ActionEvent actionEvent) {
        DirectoryChooser chooser = new DirectoryChooser();
        File file = chooser.showDialog(UIController.getPrimaryStage());
        if (file != null) {
            tfPath.setText(file.getAbsolutePath());
        }
    }

    @FXML
    public void load(ActionEvent actionEvent) {
        clear();

        showLoading();

        BuildEnvironmentTask task = new BuildEnvironmentTask(tfPath.getText());
        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                hideLoading();

                String message = task.getValue().getMessage();

                if (!new File(Environment.getInstance().getZ3Path()).exists()) {
                    Alert alert = UIController.showDialog(Alert.AlertType.CONFIRMATION, "Z3 Path is not set", "Warning");
                    Optional<ButtonType> action = alert.showAndWait();
                    if (action.isPresent()) {
                        if (action.get() == ButtonType.OK) {
                            onBuildSuccess(message);
                        }
                    }
                } else
                    onBuildSuccess(message);
            }
        });

        new VFPThread(task).start();
    }

    private void onBuildSuccess(String linkMessage) {
        Alert alert = UIController.showDialog(Alert.AlertType.CONFIRMATION, linkMessage, "Warning");
        Optional<ButtonType> action = alert.showAndWait();
        if (action.isPresent()) {
            if (action.get() == ButtonType.OK) {
                PostProcessTask processTask = new PostProcessTask();
                showLoading();
                processTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                    @Override
                    public void handle(WorkerStateEvent event) {
                        hideLoading();
                        lvFiles.getItems().setAll(Environment.getInstance().getUUTs());
                    }
                });
                new VFPThread(processTask).start();
            }
        }

        final INode root = Environment.getInstance().getProjectNode();
        List<IFunctionNode> functionNodes = Search.searchNodes(root, new FunctionNodeCondition());
        functionNodes.forEach(f -> {
            boolean isEnum = f.getArguments().stream().anyMatch(v -> VariableTypeUtils.isEnumNode(v.getRealType(), root));
            if (isEnum)
                System.out.println(f.getName());
        });
    }

    private void clear() {
        lvFiles.getItems().clear();
        lvFunctions.getItems().clear();
        lvTestCases.getItems().clear();
        coverage = null;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance = this;

        lvFiles.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                INode file = lvFiles.getSelectionModel().getSelectedItem();
                List<IFunctionNode> functionNodes = Search.searchNodes(file, new AbstractFunctionNodeCondition());
                int size = functionNodes.size();
                functionNodes.removeIf(f -> f.getArguments().stream().anyMatch(v -> isVoidOrFuncPointers(v)));
                System.out.printf("FILTER %d -> %d", size, functionNodes.size());
                lvFunctions.getItems().setAll(functionNodes);
            }
        });

        lvTestCases.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    TestCase testCase = lvTestCases.getSelectionModel().getSelectedItem();
                    UIController.viewTestData(testCase);
                }
            }
        });

        AbstractVFPTask<Void> mem = new AbstractVFPTask<Void>() {
            @Override
            protected Void call() throws Exception {
                while (!isCancelled()) {
                    update();
                    Thread.sleep(1000);
                }
                return null;
            }

            private void update() {
                long mb = MetricsTimer.getCurrentUsedMemory();
                String mem = formatSize(mb);
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        lMem.setText(mem);
                    }
                });
            }

            private String formatSize(long v) {
                if (v < 1024) return v + " B";
                int z = (63 - Long.numberOfLeadingZeros(v)) / 10;
                return String.format("%.1f %sB", (double)v / (1L << (z*10)), " KMGTPE".charAt(z));
            }
        };
        new VFPThread(mem).start();
    }

    private boolean isVoidOrFuncPointers(IVariableNode v) {
        String type = v.getRealType();

        if (VariableTypeUtils.isVoidPointer(type) || VariableTypeUtils.isFunctionPointer(type))
            return true;

//        if (v.resolveCoreType() instanceof StructureNode) {
//            StructureNode structureNode = (StructureNode) v.resolveCoreType();
//            for (IVariableNode child : structureNode.getPublicAttributes()) {
//                if (isVoidOrFuncPointers(child))
//                    return true;
//            }
//        }

        return false;
    }

    public void updateCoverage(CoverageDataObject coverage) {
        hideLoading();

        this.coverage = coverage;

        Platform.runLater(() -> {
            tCoverage.setText(String.valueOf(coverage.getProgress()));
        });
    }

    public void loadTestCases(List<TestCase> testCases) {
        Platform.runLater(() -> {
            lvTestCases.getItems().setAll(testCases);
        });
    }

    public static BaseController getInstance() {
        return instance;
    }

    @FXML
    public void highlight(ActionEvent actionEvent) {
        if (coverage != null) {
            UIController.viewHighlight(coverage);
        }
    }

    public void showLoading() {
        Platform.runLater(() -> {
            pLoading.setVisible(true);
        });
    }

    public void hideLoading() {
        Platform.runLater(() -> {
            pLoading.setVisible(false);
        });
    }

    @FXML
    public void generateAll(ActionEvent actionEvent) {
        for (INode file : Environment.getInstance().getUUTs()) {
            List<IFunctionNode> functionNodes = Search.searchNodes(file, new FunctionNodeCondition());
            for (IFunctionNode functionNode : functionNodes) {
                generate(functionNode);
            }
        }
    }
}
