package gui;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.util.Callback;
import testcase_manager.IDataTestItem;
import testdata.object.DataNode;
import util.VFPLogger;

public class InputColumnCellFactory implements Callback<TreeTableColumn<DataNode, String>, TreeTableCell<DataNode, String>> {

    private final static VFPLogger logger = VFPLogger.get(InputColumnCellFactory.class);

    private IDataTestItem testCase;

    public InputColumnCellFactory(IDataTestItem testCase) {
        super();
        this.testCase = testCase;
    }

    @Override
    public TreeTableCell<DataNode, String> call(TreeTableColumn<DataNode, String> param) {
        return new InputColumnCell(testCase);
    }

    /**
     * Represents a single row/column in the test case tab
     */
    private class InputColumnCell extends AbstractTableCell {

        public InputColumnCell(IDataTestItem testCase) {
            super(testCase);
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            setEditable(false);

            TreeItem<DataNode> treeItem = getTreeTableRow().getTreeItem();
            if (treeItem != null && treeItem.getValue() != null) {
                DataNode dataNode = treeItem.getValue();
                inputCellHandler.update(this, dataNode);
            } else {
                setText(null);
                setGraphic(null);
            }
        }

    }
}