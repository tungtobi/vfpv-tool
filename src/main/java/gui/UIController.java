package gui;

import coverage.CoverageDataObject;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import testcase_manager.TestCase;

public class UIController {

    private static Stage primaryStage;

    public static void setPrimaryStage(Stage primaryStage) {
        UIController.primaryStage = primaryStage;
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    public static Alert showDialog(Alert.AlertType type, String content, String title) {
        Alert alert = new Alert(type, content, ButtonType.OK);
        alert.setTitle(title);
        alert.initOwner(primaryStage);
        return alert;
    }

    public static void viewTestData(TestCase testCase) {
        FXMLLoader treeTableLoader = new FXMLLoader(Object.class.getResource("/fxml/TestCaseTreeTableView.fxml"));
        try {
            AnchorPane treeTable = treeTableLoader.load();
            TestCaseTreeTableController testCaseTreeTableController = treeTableLoader.getController();
            testCaseTreeTableController.loadTestCase(testCase);

            Scene scene = new Scene(treeTable);
            Stage stage = new Stage();
            stage.setTitle(testCase.getName());
            stage.setScene(scene);
            stage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void viewHighlight(CoverageDataObject coverageData) {
        FXMLLoader loader = new FXMLLoader(Object.class.getResource("/fxml/CoverageView.fxml"));
        try {
            AnchorPane view = loader.load();
            CoverageViewTabController controller = loader.getController();
            controller.loadContentToCoverageView(coverageData.getContent());

            Scene scene = new Scene(view);
            Stage stage = new Stage();
            stage.setTitle("Coverage Highlight");
            stage.setScene(scene);
            stage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
