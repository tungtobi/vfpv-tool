package gui;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import testcase_manager.IDataTestItem;
import testdata.Iterator;
import testdata.object.DataNode;
import testdata.object.GlobalRootDataNode;
import testdata.object.SubprogramNode;
import testdata.object.ValueDataNode;
import util.VFPLogger;

public class ParameterColumnCellFactory implements Callback<TreeTableColumn<DataNode, Boolean>, TreeTableCell<DataNode, Boolean>> {

    private final static VFPLogger logger = VFPLogger.get(ParameterColumnCellFactory.class);

    private final IDataTestItem testCase;

    public ParameterColumnCellFactory(IDataTestItem testCase) {
        super();
        this.testCase = testCase;
    }

    @Override
    public TreeTableCell<DataNode, Boolean> call(TreeTableColumn<DataNode, Boolean> param) {
        return new ParameterColumnCell(testCase);
    }

    /**
     * Represents a single row/column in the test case tab
     */
    public static class ParameterColumnCell extends CheckBoxTreeTableCell<DataNode, Boolean> {
        private final IDataTestItem testCase;

        private ObservableValue<Boolean> booleanProperty;

        private BooleanProperty indeterminateProperty;

        public ParameterColumnCell(IDataTestItem testCase) {
            this.testCase = testCase;
        }

        private String getDisplayName(DataNode dataNode) {
            String displayName = dataNode.getDisplayNameInParameterTree();

            TreeItem<DataNode> treeItem = getTreeTableRow().getTreeItem();
            TreeItem<DataNode> parentTreeItem = treeItem.getParent();
            DataNode parentNode = parentTreeItem.getValue();

            if (parentNode.getDisplayNameInParameterTree().equals(displayName)) {
                if (parentNode instanceof ValueDataNode) {
                    Iterator firstIterator = ((ValueDataNode) parentNode).getIterators().get(0);
                    if (dataNode == firstIterator.getDataNode()) {
                        displayName = firstIterator.getDisplayName();
                    }
                }
            }

            return displayName;
        }

        private void updateDisplay(DataNode dataNode) {
            if (dataNode != null) {
                String displayName = getDisplayName(dataNode);
                Label label = new Label(displayName);

                setGraphic(label);
                label.requestFocus();

            } else {
                logger.debug("There is no matching between a cell and a data node");
            }
        }

        private void bindingCheckbox(DataNode node, CheckBox checkBox) {
            // uninstall bindings
            if (booleanProperty != null) {
                checkBox.selectedProperty().unbindBidirectional((BooleanProperty)booleanProperty);
            }
            if (indeterminateProperty != null) {
                checkBox.indeterminateProperty().unbindBidirectional(indeterminateProperty);
            }

            // install new bindings.
            // this can only handle TreeItems of type CheckBoxTreeItem
            TreeItem<DataNode> treeItem = getTreeTableRow().getTreeItem();

            if (treeItem instanceof CheckBoxTreeItem) {
                CheckBoxTreeItem<DataNode> cbti = (CheckBoxTreeItem<DataNode>) treeItem;
                booleanProperty = new SimpleBooleanProperty(!node.getChildren().isEmpty());
                checkBox.selectedProperty().bindBidirectional((BooleanProperty)booleanProperty);

                indeterminateProperty = cbti.indeterminateProperty();
                checkBox.indeterminateProperty().bindBidirectional(indeterminateProperty);
            }
        }

        @Override
        public void startEdit() {
            logger.debug("Start editing on the cell at line " + this.getIndex());
            super.startEdit();

            setText(null);

//            TreeItem<DataNode>
            DataNode dataNode = getTreeTableRow().getTreeItem().getValue();
            updateDisplay(dataNode);
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            getTreeTableView().refresh();
            logger.debug("Canceled the edit on the cell");
        }

        @Override
        public void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            setEditable(false);

            if (getTreeTableRow().getTreeItem() != null) {
                DataNode dataNode = getTreeTableRow().getTreeItem().getValue();
                updateDisplay(dataNode);
            } else {
                setText(null);
                setGraphic(null);
            }
        }

        private void changeViewMode(GlobalRootDataNode root, boolean mode) {
            root.setShowRelated(mode);

            TreeItem<DataNode> treeItem = getTreeTableRow().getTreeItem();
            TestCaseTreeTableController.loadChildren(treeItem);

            getTreeTableView().refresh();
            logger.debug("Refreshed the current test case tab");
        }
    }
}