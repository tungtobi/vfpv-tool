package gui;

import boundary.BoundaryManager;
import config.WorkspaceConfig;
import environment.Environment;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

public class AppStart extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        WorkspaceConfig.getInstance().initialize();
        BoundaryManager.getInstance().loadExistedBoundaries();
        UIController.setPrimaryStage(primaryStage);

        primaryStage.setTitle("VFP Automation Tool");
        FXMLLoader loader;
        loader = new FXMLLoader(Object.class.getResource("/fxml/Base.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        primaryStage.setScene(scene);

        // set size to maximized and not resizable
        primaryStage.setResizable(false);
        primaryStage.show();

        // close VFP
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        // Shut down all automated test data generation threads
                        System.exit(0);
                    }
                });
            }
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
