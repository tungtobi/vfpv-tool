package gui;

import javafx.beans.property.SimpleStringProperty;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import parser.object.FunctionPointerTypeNode;
import parser.object.ICommonFunctionNode;
import parser.object.IVariableNode;
import testcase_manager.IDataTestItem;
import testdata.object.*;
import util.NodeType;
import util.TemplateUtils;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class TestCaseTreeTableController implements Initializable {

    @FXML
    private TreeTableColumn<DataNode, String> typeCol;
    @FXML
    private TreeTableColumn<DataNode, Boolean> parameterCol;
    @FXML
    private TreeTableColumn<DataNode, String> inputCol;
    @FXML
    private TreeTableView<DataNode> treeTableView;

    private TreeItem<DataNode> root;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        root = new TreeItem<>(new RootDataNode());

        treeTableView.setRoot(root);
        treeTableView.setShowRoot(false);
        treeTableView.setEditable(false);
        treeTableView.getSelectionModel().setCellSelectionEnabled(true);
        treeTableView.setRowFactory(param -> {
            final TreeTableRow<DataNode> row = new TreeTableRow<>();
            row.treeItemProperty().addListener((o, oldValue, newValue) -> {
                if (newValue != null) {
                    if (row.getTreeItem() != null) {
                        row.getTreeItem().expandedProperty().addListener(event -> {
                            row.getTreeTableView().refresh();
                        });
                    }

                }
            });

            return row;
        });

        typeCol.setCellValueFactory(param -> {
            String type = null;

            DataNode node = param.getValue().getValue();

            if (node instanceof ValueDataNode) {
                type = ((ValueDataNode) node).getRealType();

                if (node instanceof FunctionPointerDataNode) {
                    FunctionPointerDataNode fpDataNode = (FunctionPointerDataNode) node;
                    FunctionPointerTypeNode typeNode = fpDataNode.getCorrespondingType();
                    if (typeNode != null)
                        type = typeNode.getFunctionName();
                }

                type = type.replaceAll("\\s+", " ");
            }

            if (type != null)
                type = TemplateUtils.deleteTemplateParameters(type);

            return new SimpleStringProperty(type);
        });
    }

    private void loadContent(IDataTestItem testCase) {
        if (testCase != null) {
            RootDataNode rootDataNode = testCase.getRootDataNode();
            root = new TreeItem<>(rootDataNode);
            root.getChildren().clear();
            loadChildren(root);
            treeTableView.setRoot(root);
        }
    }

    public void loadTestCase(IDataTestItem testCase) {
        if (testCase != null) {
            inputCol.setCellFactory(new InputColumnCellFactory(testCase));
            parameterCol.setCellFactory(new ParameterColumnCellFactory(testCase));
            loadContent(testCase);
        }
    }

    public static void loadChildren(TreeItem<DataNode> treeItem) {
        if (treeItem == null)
            return;

        treeItem.getChildren().clear();

        DataNode node = treeItem.getValue();

        for (IDataNode child : node.getChildren()) {
            DataNode n = (DataNode) child;
            if (isVisible(n)) {
                TreeItem<DataNode> item = new TreeItem<>(n);
                treeItem.getChildren().add(item);
                if (n.getChildren() != null) {
                    loadChildren(item);
                }
            }
        }
    }

    public static boolean isParameter(IDataTestItem testCase, ValueDataNode dataNode) {
        IDataNode parent = dataNode.getParent();
        if (parent instanceof SubprogramNode) {
            // get grandparent to distinguish with parameter of a constructor of a global variable
            if (parent.getParent() instanceof UnitUnderTestNode) {
                ICommonFunctionNode sut = testCase.getFunctionNode();
                return sut == ((SubprogramNode) parent).getFunctionNode();
            }
        } else if (parent instanceof RootDataNode) {
            RootDataNode root = (RootDataNode) dataNode.getParent();
            NodeType level = root.getLevel();
            return level == NodeType.STATIC;
        }

        return false;
    }

    private static boolean isGlobalVariable(ValueDataNode dataNode) {
        if (dataNode.getParent() instanceof RootDataNode) {
            RootDataNode root = (RootDataNode) dataNode.getParent();
            NodeType level = root.getLevel();
            return level == NodeType.GLOBAL;// || level == NodeType.STATIC;
        }

        return false;
    }

    private static boolean isVisible(DataNode node) {
        if (node instanceof RootDataNode) {
            return !node.getChildren().isEmpty();
        }

        if (node instanceof ValueDataNode) {
            IDataNode parent = node.getParent();
            if (parent instanceof GlobalRootDataNode) {
                GlobalRootDataNode globalRoot = (GlobalRootDataNode) parent;
                if (globalRoot.isShowRelated()) {
                    IVariableNode v = ((ValueDataNode) node).getCorrespondingVar();
                    return globalRoot.isRelatedVariable(v);
                }
            }
        }

        return true;
    }
}
