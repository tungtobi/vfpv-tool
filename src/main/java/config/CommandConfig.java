package config;

import util.CompilerUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommandConfig implements ICommandConfig {

    private Map<String, String> compilationCommands = new HashMap<>();

    private LinkEntry linkEntry;

    private String executablePath = "";

    public CommandConfig setCompilationCommands(Map<String, String> compilationCommands) {
        this.compilationCommands = compilationCommands;
        return this;
    }

    @Override
    public Map<String, String> getCompilationCommands() {
        return compilationCommands;
    }

    @Override
    public String getLinkingCommand() {
        String linkCommand = linkEntry.getCommand();
        String outFlag = linkEntry.getOutFlag();
        String exePath = linkEntry.getExeFile();
        String[] binPaths = linkEntry.getBinFiles().toArray(new String[0]);

        return CompilerUtils.generateLinkCommand(linkCommand, outFlag, exePath, binPaths);
    }

    @Override
    public String getExecutablePath() {
        return executablePath;
    }

    public CommandConfig setExecutablePath(String executablePath) {
        this.executablePath = executablePath;
        return this;
    }

    public CommandConfig setLinkEntry(LinkEntry linkEntry) {
        this.linkEntry = linkEntry;
        return this;
    }

    public static class LinkEntry {

        private String command;
        private String outFlag;
        private String exeFile;
        private List<String> binFiles = new ArrayList<>();

        public String getCommand() {
            return command;
        }

        public void setCommand(String command) {
            this.command = command;
        }

        public String getOutFlag() {
            return outFlag;
        }

        public void setOutFlag(String outFlag) {
            this.outFlag = outFlag;
        }

        public String getExeFile() {
            return exeFile;
        }

        public void setExeFile(String exeFile) {
            this.exeFile = exeFile;
        }

        public List<String> getBinFiles() {
            return binFiles;
        }

        public void setBinFiles(List<String> binFiles) {
            this.binFiles = binFiles;
        }
    }

}
