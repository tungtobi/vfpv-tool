package search.condition;

import parser.object.CppFileNode;
import parser.object.INode;
import search.SearchCondition;

public class CppFileNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof CppFileNode;
    }
}
