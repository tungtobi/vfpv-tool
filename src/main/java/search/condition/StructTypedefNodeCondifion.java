package search.condition;

import parser.object.INode;
import parser.object.StructTypedefNode;
import search.SearchCondition;

public class StructTypedefNodeCondifion extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof StructTypedefNode;
    }
}
