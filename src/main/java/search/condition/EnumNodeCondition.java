package search.condition;

import parser.object.EnumNode;
import parser.object.EnumTypedefNode;
import parser.object.INode;
import parser.object.SpecialEnumTypedefNode;
import search.SearchCondition;

/**
 * Demo a condition
 *
 * 
 */
public class EnumNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof EnumNode || n instanceof SpecialEnumTypedefNode || n instanceof EnumTypedefNode;
    }
}
