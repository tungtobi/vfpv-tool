package search.condition;

import parser.object.INode;
import parser.object.UnknowObjectNode;
import search.SearchCondition;

public class UnknownFileNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof UnknowObjectNode;
    }
}
