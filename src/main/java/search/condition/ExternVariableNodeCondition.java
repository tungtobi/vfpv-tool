package search.condition;

import parser.object.INode;
import parser.object.IVariableNode;
import parser.object.VariableNode;
import search.SearchCondition;

/**
 * Represent extern variable, e.g., "extern int MY_MAX_VALUE"
 *
 *
 */
public class ExternVariableNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof VariableNode && ((IVariableNode) n).isExtern();
    }
}
