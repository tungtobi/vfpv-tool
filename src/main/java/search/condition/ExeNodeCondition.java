package search.condition;

import parser.object.ExeNode;
import parser.object.INode;
import search.SearchCondition;

public class ExeNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof ExeNode;
    }
}
