package search.condition;

import parser.object.INode;
import parser.object.ISourcecodeFileNode;
import search.SearchCondition;

public class HeaderNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof ISourcecodeFileNode;
    }
}
