package search.condition;

import parser.object.AbstractFunctionNode;
import parser.object.INode;
import search.SearchCondition;

public class AbstractFunctionNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof AbstractFunctionNode;
    }
}
