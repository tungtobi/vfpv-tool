package search.condition;

import parser.object.EnumTypedefNode;
import parser.object.INode;
import search.SearchCondition;

/**
 * Created by DucToan on 14/07/2017.
 */
public class EnumTypedefNodeCondifion extends SearchCondition {
    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof EnumTypedefNode;
    }
}
