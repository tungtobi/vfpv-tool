package search.condition;

import parser.object.INode;
import parser.object.IncludeHeaderNode;
import search.SearchCondition;

public class IncludeHeaderNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof IncludeHeaderNode;
    }
}
