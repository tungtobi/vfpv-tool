package search.condition;

import parser.object.INode;
import parser.object.SpecialStructTypedefNode;
import parser.object.StructNode;
import parser.object.StructTypedefNode;
import search.SearchCondition;

/**
 * Demo a condition
 *
 *
 */
public class StructNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof StructNode || n instanceof SpecialStructTypedefNode || n instanceof StructTypedefNode;
    }
}
