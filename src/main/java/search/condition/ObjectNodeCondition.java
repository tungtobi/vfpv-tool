package search.condition;

import parser.object.*;
import search.SearchCondition;

public class ObjectNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof ObjectNode;
    }

}
