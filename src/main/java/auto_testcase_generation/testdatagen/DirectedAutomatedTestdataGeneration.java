package auto_testcase_generation.testdatagen;

import auto_testcase_generation.track.MemoryTracker;
import auto_testcase_generation.track.MetricsTimer;
import auto_testcase_generation.track.TimeTracker;
import auto_testcase_generation.cfg.ICFG;
import auto_testcase_generation.cfg.object.BranchInCFG;
import auto_testcase_generation.cfg.object.ConditionCfgNode;
import auto_testcase_generation.cfg.object.ICfgNode;
import auto_testcase_generation.cfg.object.NormalCfgNode;
import auto_testcase_generation.cfg.testpath.FullTestpath;
import auto_testcase_generation.cfg.testpath.ITestpathInCFG;
import auto_testcase_generation.testdatagen.advanced_pointer.TypeMap;
import auto_testcase_generation.testdatagen.advanced_pointer.TypeMapEvaluator;
import auto_testcase_generation.testdatagen.se.ISymbolicExecution;
import auto_testcase_generation.testdatagen.se.NewVariableInSe;
import auto_testcase_generation.testdatagen.se.Parameters;
import auto_testcase_generation.testdatagen.se.SymbolicExecution;
import auto_testcase_generation.testdatagen.se.solver.RunZ3OnCMD;
import auto_testcase_generation.testdatagen.se.solver.SmtLibGeneration;
import auto_testcase_generation.testdatagen.se.solver.solutionparser.Z3SolutionParser;
import auto_testcase_generation.testdatagen.testdatainit.BasicTypeRandom;
import config.FunctionConfig;
import config.WorkspaceConfig;
import coverage.CoverageDataObject;
import coverage.CoverageManager;
import coverage.SourcecodeCoverageComputation;
import environment.EnviroCoverageTypeNode;
import environment.Environment;
import gui.BaseController;
import gui.UIController;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.jgrapht.Graph;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import parser.object.*;
import testcase_execution.TestcaseExecution;
import testcase_manager.ITestCase;
import testcase_manager.TestCase;
import testcase_manager.TestCaseManager;
import testcase_manager.TestPrototype;
import testcase_manager.minimize.GreedyMinimizer;
import testcase_manager.minimize.ITestCaseMinimizer;
import testcase_manager.minimize.Scope;
import testdata.gen.module.SimpleTreeDisplayer;
import testdata.object.RootDataNode;
import util.*;

import java.io.File;
import java.util.*;

import static auto_testcase_generation.testdatagen.RandomInputGeneration.OPTION_VOID_POINTER_PRIMITIVE_TYPES;
import static auto_testcase_generation.testdatagen.RandomInputGeneration.OPTION_VOID_POINTER_STRUCTURE_TYPES;

public class DirectedAutomatedTestdataGeneration extends AbstractAutomatedTestdataGeneration {
    private final static VFPLogger logger = VFPLogger.get(DirectedAutomatedTestdataGeneration.class);
    private TypeMap typeMap;

    public DirectedAutomatedTestdataGeneration(ICommonFunctionNode fn, String coverageType) {
        super(fn);
        this.coverageType = coverageType;
    }

    public static String toDescription(ICfgNode node) {
        return String.format("%s(id = %s)", node.getContent(), node.getId());
    }

    public static List<ICfgNode> findShortestTestpath(ICfgNode target, ICFG currentCFG) {
        List<ICfgNode> shortestPath = new ArrayList<>();
        Map<String, ICfgNode> mapping = new HashMap<>();

        Graph<String, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);

        for (ICfgNode node : currentCFG.getExpandedNodes()) {
            String name = toDescription(node);
            graph.addVertex(name);

            mapping.put(name, node);
        }

        constructGraphEdges(graph, currentCFG);

        // Find shortest paths
        logger.debug("Shortest path from begin node of CFG to " + target + " :");
        DijkstraShortestPath dijkstraAlg = new DijkstraShortestPath<>(graph);
        ICfgNode beginNode = currentCFG.getBeginNode();
        ShortestPathAlgorithm.SingleSourcePaths<String, DefaultEdge> iPaths = dijkstraAlg.getPaths(toDescription(beginNode));
        org.jgrapht.GraphPath<String, DefaultEdge> shortestTestPath = iPaths.getPath(toDescription(target));

        for (String str : shortestTestPath.getVertexList()) {
            ICfgNode n = mapping.get(str);
            shortestPath.add(n);
        }

        // Normalize path
        List<ICfgNode> normalizedShortestPath = new ArrayList<>();
        for (int i = 0; i < shortestPath.size(); i++) {
            ICfgNode n = shortestPath.get(i);
            if (n instanceof ConditionCfgNode) {
                if (i + 1 < shortestPath.size()) {
                    ICfgNode next = shortestPath.get(i + 1);
                    if (n.getTrueNode().getId() == next.getId()) {
                        normalizedShortestPath.add(n);
                    } else {
                        ConditionCfgNode clone = new ConditionCfgNode(((ConditionCfgNode) n).getAst()) {
                            @Override
                            public boolean isVisitedTrueBranch() {
                                return super.isVisitedTrueBranch();
                            }
                        };
                        clone.setContent("!(" + n.getContent() + ")");
                        clone.setAst(Utils.convertToIAST(clone.getContent()));
                        normalizedShortestPath.add(clone);
                    }
                }
            } else {
                normalizedShortestPath.add(n);
            }
        }

        logger.debug("shortestPath = " + shortestPath);
        logger.debug("normalizedShortestPath = " + normalizedShortestPath);
        return normalizedShortestPath;
    }

    private static void constructGraphEdges(Graph<String, DefaultEdge> graph, ICFG currentCFG) {
        for (ICfgNode node : currentCFG.getExpandedNodes()) {
            ICfgNode trueNode = node.getTrueNode();
            ICfgNode falseNode = node.getFalseNode();

            if (trueNode != null && falseNode != null) {
                if (!trueNode.equals(falseNode)) {
                    addEdge(graph, node, trueNode);
                    addEdge(graph, node, falseNode);
                } else
                    addEdge(graph, node, trueNode);
            } else if (trueNode != null) {
                addEdge(graph, node, trueNode);
            } else if (falseNode != null) {
                addEdge(graph, node, falseNode);
            }
        }
    }

    private static void addEdge(Graph<String, DefaultEdge> graph, ICfgNode from, ICfgNode to) {
        if (to instanceof NormalCfgNode && !((NormalCfgNode) to).getSubCFGs().isEmpty()) {
            Collection<ICFG> subCFGs = ((NormalCfgNode) to).getSubCFGs().values();
            ICfgNode prevNode = from;
            for (ICFG subCFG : subCFGs) {
                ICfgNode newTo = subCFG.getBeginNode();
                graph.addEdge(toDescription(prevNode), toDescription(newTo));
                prevNode = subCFG.getAllNodes().get(subCFG.getAllNodes().size() - 1);
            }
            graph.addEdge(toDescription(prevNode), toDescription(to));
        } else {
            graph.addEdge(toDescription(from), toDescription(to));
        }
    }

    public void generateTestdata(ICommonFunctionNode fn) throws Exception {
        logger.debug("Generating test data for function " + fn.getName());
        logger.debug("Automated test data generation strategy: Directed-Dijkstra");

        if (fn.getFunctionConfig() == null) {
            FunctionConfig functionConfig = Environment.getInstance().getDefaultFunctionConfig();
            functionConfig.setFunctionNode(fn);
            fn.setFunctionConfig(functionConfig);
            functionConfig.createBoundOfArgument(functionConfig, fn);
        }

        final long MAX_ITERATON = fn.getFunctionConfig().getTheMaximumNumberOfIterations(); // may change to any value
        logger.debug("Maximum number of iterations = " + MAX_ITERATON);

        if (fn.isTemplate()
//                || fn.hasVoidPointerArgument() || fn.hasFunctionPointerArgument()
        ) {
            if (this.allPrototypes.size() == 0)
                return;
        }

        testCases = TestCaseManager.getTestCasesByFunction(this.fn);

        start(this.testCases, this.fn, this.coverageType, this.allPrototypes, this.generatedTestcases, this.analyzedTestpathMd5, showReport);
    }

    /**
     * Generate test data
     */
    protected void start(List<TestCase> testCases, ICommonFunctionNode fn, String coverageType,
                         List<TestPrototype> allPrototypes,
                         List<String> generatedTestcases,
                         List<String> analyzedTestpathMd5,
                         boolean showReport) {
        List<String> oldTestcases = new ArrayList<>();
        for (TestCase tc : testCases)
            if (tc != null && tc.getName() != null)
                oldTestcases.add(tc.getName());

        /**
         * Generate randomly (not in parallel mode)
         */
        AdvancedPointerRandomAutomatedTestdataGeneration randomGen;
        randomGen = new AdvancedPointerRandomAutomatedTestdataGeneration(fn);
        try {
            int MAX_RANDOM_TC = 15;
            logger.debug("Random test data generation");
            randomGen.setLimitNumberOfIterations(MAX_RANDOM_TC);
            randomGen.setShouldRunParallel(false);
            randomGen.setShowReport(false);
            randomGen.getAllPrototypes().addAll(allPrototypes);
            logger.debug("Generate test data automatically for function " + fn.getSimpleName());
            randomGen.generateTestdata(fn);
            typeMap = randomGen.getTypeMap();
            testCases.addAll(randomGen.getTestCases());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * Generate directly
         */

        long memory = randomGen.getMemory();
        long time = randomGen.getTime();

        if (new File(Environment.getInstance().getZ3Path()).exists()) {
            long MAX_TESTCASES = fn.getFunctionConfig().getTheMaximumNumberOfIterations();
            for (long i = 0; i < 4; i++) {
                logger.debug("Iterate " + i + " directly");
                logger.debug("Num of the existing test cases up to now = " + testCases.size());

                MetricsTimer metricsTimer = new MetricsTimer();
                metricsTimer.start();
                int iterationStatus = generateDirectly(testCases, fn, coverageType,
                        generatedTestcases, analyzedTestpathMd5, metricsTimer);
                memory += metricsTimer.getMemory();
                time += metricsTimer.getTime();

                switch (iterationStatus) {
                    case AUTOGEN_STATUS.OTHER_ERRORS: {
                        logger.debug("Unexpected error when generating test case. Nove to the next iteration.");
                        break;
                    }
                    case AUTOGEN_STATUS.NOT_ABLE_TO_GENERATE_CFG: {
                        logger.debug("Unable to generate cfg of function " + fn.getAbsolutePath() + ". Move to the next iteration.");
                        break;
                    }
                    case AUTOGEN_STATUS.NO_SHORTEST_PATH: {
                        logger.debug("Not path to discover. Exit.");
                        break;
                    }
                    case AUTOGEN_STATUS.FOUND_DUPLICATED_TESTPATH: {
                        logger.debug("The generated test path existed before. Move to the next iteration.");
                        break;
                    }
                    case AUTOGEN_STATUS.SOLVING_STATUS.FOUND_DUPLICATED_TESTCASE: {
                        logger.debug("The generated test case existed before. Move to the next iteration.");
                        break;
                    }
                    case AUTOGEN_STATUS.EXECUTION.COULD_NOT_CONSTRUCT_TREE_FROM_TESTCASE: {
                        logger.debug("Could not construct tree from generated test case. Move to the next iteration.");
                        break;
                    }
                    case AUTOGEN_STATUS.EXECUTION.COUND_NOT_EXECUTE_TESTCASE: {
                        logger.debug("Could not execute test case. Move to the text iteration.");
                        break;
                    }
                    case AUTOGEN_STATUS.EXECUTION.BE_ABLE_TO_EXECUTE_TESTCASE: {
                        logger.debug("Be able to execute test case. Save. Move to the text iteration.");
                        break;
                    }
                }
            }
        }


        TimeTracker.getInstance().append(fn, time);
//        IterationTracker.getInstance().increase(fn);

        MemoryTracker.getInstance().put(fn, memory);

        // optimize the number of test cases
        try {
            // remove the generated test cases before auto gen
            for (int i = testCases.size() - 1; i >= 0; i--)
                if (oldTestcases.contains(testCases.get(i).getName())) {
                    testCases.remove(i);
                }

            List<TestCase> newTestCases = TestCaseManager.getTestCasesByFunction(this.fn);
            CsvUtils.appendResult(fn, newTestCases);

            ITestCaseMinimizer minimizer = new GreedyMinimizer();
            newTestCases = minimizer.clean(newTestCases, Scope.SOURCE);

            // view coverage of generated set of test cases
            BaseController.getInstance().loadTestCases(newTestCases);

            CoverageDataObject object = CoverageManager
                    .getCoverageOfMultiTestCaseAtFunctionLevel(newTestCases, coverageType);
            if (object != null) {
                BaseController.getInstance().updateCoverage(object);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            int nTestcases = TestCaseManager.getTestCasesByFunction(fn).size();
        }
    }

    /**
     * @param testCases           a list of existing test cases
     * @param functionNode        the tested function
     * @param cov                 the target coverage
     * @param generatedTestcases  the generated test cases
     * @param analyzedTestpathMd5 md5 of analyzed test paths
     * @return status of test case generation
     */
    public int generateDirectly(List<TestCase> testCases, ICommonFunctionNode functionNode, String cov,
                                List<String> generatedTestcases,
                                List<String> analyzedTestpathMd5, MetricsTimer timer) {
        String allTestpaths = "";
        for (TestCase testCase : testCases)
            if (testCase.getTestPathFile() != null && new File(testCase.getTestPathFile()).exists())
                allTestpaths += Utils.readFileContent(testCase.getTestPathFile()) + "\n";

        // coverage
        ICFG currentCFG = null;
        if (allTestpaths.length() > 0) {
            /**
             * Compute coverage of all test cases up to now
             */
            logger.debug("Start computing total coverage");
            ISourcecodeFileNode sourcecodeNode = Utils.getSourcecodeFile(functionNode);
            SourcecodeCoverageComputation sourcecodeCoverageComputation = new SourcecodeCoverageComputation();
            sourcecodeCoverageComputation.setCoverage(cov);
            sourcecodeCoverageComputation.setConsideredSourcecodeNode(sourcecodeNode);
            sourcecodeCoverageComputation.setTestpathContent(allTestpaths);
            sourcecodeCoverageComputation.compute();
            logger.debug("Total coverage computation... DONE");

            /**
             * Get cfg corresponding to the tested function
             */
            List<ICFG> CFGs = sourcecodeCoverageComputation.getAllCFG();
            for (ICFG cfg : CFGs) {
                if (cfg.getFunctionNode().getAbsolutePath().equals(functionNode.getAbsolutePath())) {
                    currentCFG = cfg;
                    break;
                }
            }
        } else {
            /**
             * There is no previous test case, just need to construct a cfg
             */
            try {
                if (functionNode instanceof AbstractFunctionNode) {
                    currentCFG = CFGUtils.createCFG((IFunctionNode) functionNode, cov);
                    currentCFG.setFunctionNode((IFunctionNode) functionNode);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (currentCFG != null) {
            List<ICfgNode> shortestTestpath = getShortestPathByCoverage(currentCFG, cov);

            if (shortestTestpath.size() == 0) {
                timer.stop();
                return AUTOGEN_STATUS.NO_SHORTEST_PATH;
            } else
                return solve(shortestTestpath, functionNode, generatedTestcases, analyzedTestpathMd5, timer);
        } else {
            timer.stop();
            return AUTOGEN_STATUS.NOT_ABLE_TO_GENERATE_CFG;
        }
    }

    /**
     * Find a test case traversing the given test path
     *
     * @param testpath            a test path
     * @param fn                  the tested function
     * @param generatedTestcases  generated test cases
     * @param analyzedTestpathMd5 md5 of analyzed test paths (result of executing the generated test case)
     * @return state of solving process (FOUND_DUPLICATED_TESTCASE/ COULD_NOT_CONSTRUCT_TREE_FROM_TESTCASE/
     * COUND_NOT_EXECUTE_TESTCASE/ BE_ABLE_TO_EXECUTE_TESTCASE)
     */
    protected int solve(List<ICfgNode> testpath, ICommonFunctionNode fn, List<String> generatedTestcases,
                        List<String> analyzedTestpathMd5, MetricsTimer timer) {
        String tpStr = testpath.toString();
        String md5 = Utils.computeMd5(tpStr);
        if (analyzedTestpathMd5.contains(md5)) {
            timer.stop();
            return AUTOGEN_STATUS.FOUND_DUPLICATED_TESTPATH;
        }

        analyzedTestpathMd5.add(md5);

        /**
         * Get arguments + global variables
         */
        Parameters parameters = new Parameters();
        List<IVariableNode> globalVars = fn.getArgumentsAndGlobalVariables();
        parameters.addAll(globalVars);
        logger.debug("List of params: " + globalVars);

        List<RandomValue> theNextTestdata = generateTheNextTestData(testpath, fn, generatedTestcases);
        timer.stop();

        if (theNextTestdata != null && theNextTestdata.size() > 0) {
            /**
             * Initialize a test case
             */
            String nameofTestcase = TestCaseManager.generateContinuousNameOfTestcase(fn.getSimpleName() + ITestCase.POSTFIX_TESTCASE_BY_DIRECTED_METHOD);
            TestCase testCase = TestCaseManager.createTestCase(nameofTestcase, fn);

            /**
             * Execute a test case
             */
            if (testCase != null) {
                TestcaseExecution executor = new TestcaseExecution();
                executor.setFunction(fn);
                executor.setMode(TestcaseExecution.IN_AUTOMATED_TESTDATA_GENERATION_MODE);
                int executionStatus = iterateDirectly(testCase, executor, fn, theNextTestdata);
                return executionStatus;
            } else
                return AUTOGEN_STATUS.OTHER_ERRORS;

        } else {
            timer.stop();
            logger.debug("There is no next test data");
            return AUTOGEN_STATUS.SOLVING_STATUS.FOUND_DUPLICATED_TESTCASE;
        }
    }

    protected List<ICfgNode> getShortestPathByCoverage(ICFG currentCFG, String coverageType) {
        currentCFG.setIdforAllNodes();

        switch (coverageType) {
            case EnviroCoverageTypeNode.STATEMENT: {
                logger.debug("Visited stm = " + currentCFG.getVisitedStatements().size());
                int nStm = currentCFG.getVisitedStatements().size() + currentCFG.getUnvisitedStatements().size();
                logger.debug("Total stm = " + nStm);

                if (currentCFG.getUnvisitedStatements().size() == 0) {
                    maximizeCov = true;
                    return new ArrayList<>();
                } else {
                    ICfgNode unvisitedInstructions = currentCFG.getUnvisitedStatements().get(
                            new BasicTypeRandom().generateInt(0, currentCFG.getUnvisitedStatements().size() - 1));
                    logger.debug("Choose unvisited stm \"" + unvisitedInstructions + "\"");

                    // find a shortest test path through unvisited instructions
                    List<ICfgNode> shortestTestpath = findShortestTestpath(unvisitedInstructions, currentCFG);
                    logger.debug("Shortest test path: " + shortestTestpath);

                    return shortestTestpath;
                }
            }
            case EnviroCoverageTypeNode.BRANCH:
            case EnviroCoverageTypeNode.MCDC: {
                logger.debug("Visited branches = " + currentCFG.getVisitedBranches().size());
                int nStm = currentCFG.getVisitedBranches().size() + currentCFG.getUnvisitedBranches().size();
                logger.debug("Total branches = " + nStm);

                if (currentCFG.getUnvisitedBranches().size() == 0) {
                    maximizeCov = true;
                    return new ArrayList<>();
                } else {
                    BranchInCFG selectedBranch = currentCFG.getUnvisitedBranches().get(
                            new BasicTypeRandom().generateInt(0, currentCFG.getUnvisitedBranches().size() - 1));
                    ICfgNode unvisitedInstructions = selectedBranch.getEnd();
                    logger.debug("Choose unvisited branches \"" + selectedBranch.getStart() + "\" -> \"" + selectedBranch.getEnd() + "\"");

                    // find a shortest test path through unvisited instructions
                    List<ICfgNode> shortestTestpath = findShortestTestpath(unvisitedInstructions, currentCFG);
                    logger.debug("Shortest test path: " + shortestTestpath);

                    return shortestTestpath;
                }
            }

            case EnviroCoverageTypeNode.STATEMENT_AND_BRANCH: {
                // ignore
                break;
            }
            case EnviroCoverageTypeNode.STATEMENT_AND_MCDC: {
                // ignore
                break;
            }
            case EnviroCoverageTypeNode.BASIS_PATH: {
                // ignore
                break;
            }
        }
        return new ArrayList<>();
    }

    /**
     * @param testCase
     * @param executor
     * @param fn
     * @param theNextTestdata
     * @return COULD_NOT_CONSTRUCT_TREE_FROM_TESTCASE/ COUND_NOT_EXECUTE_TESTCASE/ BE_ABLE_TO_EXECUTE_TESTCASE
     */
    protected int iterateDirectly(TestCase testCase, TestcaseExecution executor, ICommonFunctionNode fn,
                                  List<RandomValue> theNextTestdata) {
        RootDataNode root = testCase.getRootDataNode();

        try {
            logger.debug("recursiveExpandUutBranch");
            recursiveExpandUutBranch(root.getRoot(), theNextTestdata, testCase);
        } catch (Exception e) {
            e.printStackTrace();
            return AUTOGEN_STATUS.EXECUTION.COULD_NOT_CONSTRUCT_TREE_FROM_TESTCASE;
        }

        try {
            logger.debug(new SimpleTreeDisplayer().toString(root));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return executeTestCase(testCase, executor, "");
    }

    protected int executeTestCase(TestCase testCase, TestcaseExecution executor, String additionalHeaders) {
        try {
            testCase.setStatus(TestCase.STATUS_EXECUTING);
            testCase.setAdditionalHeaders(additionalHeaders);

            String coverage = Environment.getInstance().getTypeofCoverage();

            // add and initialize corresponding TestCaseExecutionDataNode
            ICommonFunctionNode functionNode = testCase.getFunctionNode();

            // Execute random values
            executor.setTestCase(testCase);
            executor.execute();
            // save test case to file
            logger.debug("Save the testcase " + testCase.getName());

            if (testCase.getStatus().equals(TestCase.STATUS_SUCCESS)) {
                // read coverage information from file to display on GUI
                testCases.add(testCase);
                // export coverage of testcase to file
                return AUTOGEN_STATUS.EXECUTION.BE_ABLE_TO_EXECUTE_TESTCASE;
            } else {
                logger.debug("Do not add test case " + testCase.getName() + " because we can not execute it");
                return AUTOGEN_STATUS.EXECUTION.COUND_NOT_EXECUTE_TESTCASE;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return AUTOGEN_STATUS.EXECUTION.COUND_NOT_EXECUTE_TESTCASE;
        }
    }

    protected List<RandomValue> generateTheNextTestData(List<ICfgNode> shortestTestpath, ICommonFunctionNode functionNode,
                                                        List<String> generatedTestcases) {
        List<RandomValue> theNextTestdata = new ArrayList<>();

        Parameters paramaters = new Parameters();
        paramaters.addAll(functionNode.getArgumentsAndGlobalVariables());

        try {
            ITestpathInCFG testpathInCFG = new FullTestpath();
            testpathInCFG.getAllCfgNodes().addAll(shortestTestpath);
            ISymbolicExecution se = new SymbolicExecution(testpathInCFG, paramaters, functionNode);
            logger.debug("Constraints: " + se.getConstraints());
            logger.debug("new variables: " + se.getNewVariables().toString());
            logger.debug("table mapping: " + se.getTableMapping().toString());

            // add new variables to parameters
            for (NewVariableInSe newVar : se.getNewVariables()) {
                boolean isExist = paramaters.stream().anyMatch(v -> v.getName().equals(newVar.getOriginalName()));
                if (!isExist) {
                    VariableNode var = new VariableNode();
                    var.setRawType(VariableTypeUtils.BASIC.NUMBER.INTEGER.INT);
                    var.setCoreType(VariableTypeUtils.BASIC.NUMBER.INTEGER.INT);
                    var.setName(newVar.getOriginalName());
                    paramaters.add(var);
                }
            }

            // generate smt-lib2
            SmtLibGeneration smt = new SmtLibGeneration(paramaters, se.getNormalizedPathConstraints(), functionNode, se.getNewVariables());
            smt.generate();
            logger.debug("SMT-LIB file:\n" + smt.getSmtLibContent());
            String constraintFile = WorkspaceConfig.CONSTRAINT_DIR.getAbsolutePath()
                    + File.separator + new RandomDataGenerator().nextInt(0, 99999) + ".smt2";
            logger.debug("constraintFile: " + constraintFile);
            Utils.writeContentToFile(smt.getSmtLibContent(), constraintFile);

            // solve
            logger.debug("Calling solver z3");
            String z3Path = Environment.getInstance().getZ3Path();
            if (new File(z3Path).exists()) {
                RunZ3OnCMD z3Runner = new RunZ3OnCMD(z3Path, constraintFile);
                z3Runner.execute();

                logger.debug("Original solution:\n" + z3Runner.getSolution());
                String staticSolution = new Z3SolutionParser().getSolution(z3Runner.getSolution());
                for (IVariableNode param : functionNode.getArguments()) {
                    if (VariableTypeUtils.isVoidPointer(param.getRealType())) {
                        String name = param.getName();
                        String value = "selectedCoreType=int,selectedCategory=Primitive Types,level=1";
                        TypeMap typeMap = TypeMapEvaluator.evaluate(functionNode);
                        if (typeMap.containsKey(name) && !typeMap.get(name).isEmpty()) {
                            String rawType = typeMap.get(name).get(0);
                            int level = Utils.getLevel(rawType);
                            String coreType = rawType.replaceAll(IRegex.POINTER, SpecialCharacter.EMPTY);
                            String category = OPTION_VOID_POINTER_PRIMITIVE_TYPES;
                            if (VariableTypeUtils.isStructureSimple(coreType))
                                category = OPTION_VOID_POINTER_STRUCTURE_TYPES;
                            value = String.format("selectedCoreType=%s,selectedCategory=%s,level=%d", coreType, category, level);
                        }
                        RandomValue randomValue = new RandomValue(name, value);
                        theNextTestdata.add(randomValue);
                        staticSolution = staticSolution
                                .replaceAll("\\b" + name + "\\b", name + "_value");
                    }
                }
                String md5 = Utils.computeMd5(staticSolution);
                if (!generatedTestcases.contains(md5)) {
                    generatedTestcases.add(md5);

                    logger.debug("the next test data = " + staticSolution);

                    logger.debug("Convert to standard format");
                    ValueToTestcaseConverter_UnknownSize converter = new ValueToTestcaseConverter_UnknownSize(staticSolution);
                    List<RandomValue> randomValues = converter.convert();
                    logger.debug(randomValues);

                    theNextTestdata.addAll(randomValues);
                } else {
                    logger.debug("The next test data exists! Ignoring...");
                    theNextTestdata = new ArrayList<>();
                }
            } else
                throw new Exception("Z3 path " + z3Path + " does not exist");
        } catch (Exception e) {
            e.printStackTrace();
            Platform.runLater(() -> {
                Alert alert = UIController.showDialog(Alert.AlertType.ERROR, e.getMessage(), "Error");
                alert.showAndWait();
            });
        }

//        for (IVariableNode var : functionNode.getArguments()) {
//            if (VariableTypeUtils.isVoidPointer(var.getRealType())) {
//                String selectedType = VariableTypeUtils.BASIC.NUMBER.INTEGER.INT;
//                if (typeMap.containsKey(var.getName()) && !typeMap.get(var.getName()).isEmpty()) {
//                    selectedType = typeMap.get(var.getName()).get(0);
//                }
//                String selectedCoreType = selectedType.replaceAll("(\\*)+", "").trim();
//                int level = PointerTypeInitiation.getLevel(selectedType);
//                String category = null;
//
//                // check whether the selected type is primitive
//                List<INode> allPrimitiveNodes = VariableTypeUtils.getAllPrimitiveTypeNodes();
//                for (INode node : allPrimitiveNodes)
//                    if (node.getName().equals(selectedCoreType)) {
//                        category = AbstractTableCell.OPTION_VOID_POINTER_PRIMITIVE_TYPES;
//                        break;
//                    }
//
//                // check whether the selected type is structure
//                if (category == null) {
//                    selectedCoreType = VariableTypeUtils.removeRedundantKeyword(selectedCoreType);
//                    List<INode> allStructureNodes = VariableTypeUtils.getAllStructureNodes();
//                    for (INode node : allStructureNodes)
//                        if (node.getName().equals(selectedCoreType)) {
//                            category = AbstractTableCell.OPTION_VOID_POINTER_STRUCTURE_TYPES;
//                            break;
//                        }
//                }
//
//                RandomValue v = new RandomValueForAssignment(var.getName(),
//                        String.format("%s=%s,%s=%s,%s=%s",
//                                VOID_POINTER____SELECTED_CORE_TYPE, selectedCoreType,
//                                VOID_POINTER____SELECTED_CATEGORY, category,
//                                VOID_POINTER____POINTER_LEVEL, level));
//                theNextTestdata.add(v);
//            }
//        }

        return theNextTestdata;
    }
}
