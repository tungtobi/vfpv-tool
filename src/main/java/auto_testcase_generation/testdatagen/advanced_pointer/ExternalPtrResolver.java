package auto_testcase_generation.testdatagen.advanced_pointer;

import environment.Environment;
import parser.ProjectParser;
import parser.dependency.FunctionCallDependency;
import parser.dependency.finder.Level;
import parser.dependency.finder.VariableSearchingSpace;
import parser.object.*;
import resolver.NewTypeResolver;
import search.Search;
import search.SearchCondition;
import search.condition.AbstractFunctionNodeCondition;
import search.condition.DefinitionFunctionNodeCondition;
import search.condition.FunctionNodeCondition;
import util.FunctionPointerUtils;
import util.VariableTypeUtils;
import org.eclipse.cdt.core.dom.ast.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExternalPtrResolver extends PointerResolver {

    private static final int IS_VOID_POINTER = 1;
    private static final int IS_FUNCTION_POINTER = 2;

    private final int[] parameterMark;
    private final Map<IVariableNode, List<ICommonFunctionNode>> matchesMap = new HashMap<>();

    public ExternalPtrResolver(ICommonFunctionNode sut) {
        super(sut);

        List<IVariableNode> arguments = sut.getArguments();
        parameterMark = new int[arguments.size()];

        // Find all function in space
        VariableSearchingSpace searchingSpace = new VariableSearchingSpace(sut);
        List<ICommonFunctionNode> functionNodesInSpace = new ArrayList<>();
        List<Level> space = searchingSpace.getSpaces();
        for (Level level : space) {
            for (INode node : level) {
                List<SearchCondition> conditions = new ArrayList<>();
                conditions.add(new AbstractFunctionNodeCondition());
                conditions.add(new DefinitionFunctionNodeCondition());
                List<ICommonFunctionNode> functionNodes = Search.searchNodes(node, conditions);
                functionNodesInSpace.addAll(functionNodes);
            }
        }
        functionNodesInSpace = functionNodesInSpace.stream()
                .distinct()
                .collect(Collectors.toList());

        for (int i = 0; i < arguments.size(); i++) {
            IVariableNode argument = arguments.get(i);
            String type = argument.getRealType();
            if (VariableTypeUtils.isVoidPointer(type))
                parameterMark[i] = IS_VOID_POINTER;
            else if (VariableTypeUtils.isFunctionPointer(type)) {
                parameterMark[i] = IS_FUNCTION_POINTER;
                FunctionPointerTypeNode typeNode = (FunctionPointerTypeNode) argument.resolveCoreType();
                List<ICommonFunctionNode> matches;
                matches = functionNodesInSpace.stream()
                        .filter(f -> FunctionPointerUtils.match(typeNode, f))
                        .collect(Collectors.toList());
                matchesMap.put(argument, matches);
            } else
                parameterMark[i] = 0;
        }
    }

    @Override
    public TypeMap solve() {
        functionNode.getDependencies().stream()
                .filter(d -> d instanceof FunctionCallDependency && d.getEndArrow().equals(functionNode))
                .map(d -> (FunctionCallDependency) d)
                .forEach(this::handleExpression);

        return super.solve();
    }

    private void handleExpression(FunctionCallDependency d) {
        ICommonFunctionNode callee = d.getStartArrow();
        d.getExprArguments().forEach(arguments -> {
            for (int i = 0; i < parameterMark.length; i++) {
                IVariableNode parameter = functionNode.getArguments().get(i);
                if (parameterMark[i] == IS_VOID_POINTER) {
                    handleVoidPointer(callee, arguments[i], parameter);
                } else if (parameterMark[i] == IS_FUNCTION_POINTER && arguments[i] instanceof IASTExpression) {
                    handleFuncPointer(callee, arguments[i], parameter);
                }
            }
        });
    }

    private void handleFuncPointer(ICommonFunctionNode callee, IASTInitializerClause argument, IVariableNode parameter) {
        IASTFunctionDefinition ast = null;

        if (callee instanceof IFunctionNode)
            ast = ((IFunctionNode) callee).getAST();

        if (ast != null) {
            List<ICommonFunctionNode> functionNodesInSpace = matchesMap.get(parameter);
            PriorityFunctionAssignmentVisitor visitor = new PriorityFunctionAssignmentVisitor(functionNodesInSpace);
            ast.accept(visitor);
            List<ICommonFunctionNode> priority = visitor.getPriority();
            List<String> values = priority.stream()
                    .map(ICommonFunctionNode::getName)
                    .collect(Collectors.toList());
            String name = parameter.getName();
            typeMap.put(name, values);
        }
    }

    private void handleVoidPointer(ICommonFunctionNode callee, IASTInitializerClause argument, IVariableNode parameter) {
        String type = new NewTypeResolver(callee).solve(argument);
        String name = parameter.getName();
        typeMap.append(name, type);
    }

    private static class PriorityFunctionAssignmentVisitor extends ASTVisitor {

        private final List<ICommonFunctionNode> functionNodes;

        public PriorityFunctionAssignmentVisitor(List<ICommonFunctionNode> functionNodesInSpace) {
            this.functionNodes = new ArrayList<>(functionNodesInSpace);
            shouldVisitExpressions = true;
        }

        @Override
        public int visit(IASTExpression expression) {
            if (expression instanceof IASTIdExpression) {
                for (ICommonFunctionNode f : functionNodes) {
                    if (f.getSingleSimpleName().equals(expression.getRawSignature())) {
                        functionNodes.remove(f);
                        functionNodes.add(0, f);
                        break;
                    }
                }
            }

            return PROCESS_CONTINUE;
        }

        public List<ICommonFunctionNode> getPriority() {
            return functionNodes;
        }
    }
}
