package auto_testcase_generation.testdatagen.advanced_pointer;

import parser.ProjectParser;
import parser.dependency.finder.MethodFinder;
import parser.object.*;
import resolver.NewTypeResolver;
import search.Search;
import search.condition.FunctionNodeCondition;
import util.IRegex;
import util.SpecialCharacter;
import util.Utils;
import util.VariableTypeUtils;
import org.eclipse.cdt.core.dom.ast.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class InternalPtrResolver extends PointerResolver {

    private IASTFunctionDefinition astFunction;

    private final List<IVariableNode> considerVariables;

    public InternalPtrResolver(ICommonFunctionNode sut) {
        super(sut);

        if (sut instanceof AbstractFunctionNode)
            astFunction = ((AbstractFunctionNode) sut).getAST();

        considerVariables = searchVariables(0, sut);
    }

    private IVariableNode getParameter(String name, boolean isVoidPtr) {
        if (isVoidPtr) {
            return considerVariables.stream()
                    .filter(v -> VariableTypeUtils.isVoidPointer(v.getRealType()))
                    .filter(v -> v.getName().equals(name))
                    .findFirst()
                    .orElse(null);
        } else {
            return considerVariables.stream()
                    .filter(v -> VariableTypeUtils.isFunctionPointer(v.getRealType()))
                    .filter(v -> v.getName().equals(name))
                    .findFirst()
                    .orElse(null);
        }
    }

    private static final int MAX_LOOP = 10;

    private List<IVariableNode> searchVariables(int iterator, INode root) {
        List<IVariableNode> output = new ArrayList<>();

        if (iterator < MAX_LOOP) {
            List<IVariableNode> considerVariables = new ArrayList<>();

            if (root instanceof ICommonFunctionNode) {
                considerVariables = ((ICommonFunctionNode) root).getArguments();
            } else if (root instanceof IVariableNode) {
                INode typeNode = ((IVariableNode) root).resolveCoreType();
                if (typeNode instanceof StructureNode) {
                    considerVariables = ((StructureNode) typeNode).getPublicAttributes();
                    if (typeNode instanceof ClassNode) {
                        List<ICommonFunctionNode> constructors = ((ClassNode) typeNode).getConstructors();
                        for (ICommonFunctionNode constructor : constructors) {
                            output.addAll(searchVariables(iterator + 1, constructor));
                        }
                    }
                }
            }

            for (IVariableNode child : considerVariables) {
                String type = child.getRealType();
                if (VariableTypeUtils.isVoidPointer(type) || VariableTypeUtils.isFunctionPointer(type)) {
                    output.add(child);
                } else {
                    output.addAll(searchVariables(iterator + 1, child));
                }
            }
        }

        return output;
    }

    public TypeMap solve() {
        if (astFunction != null) {
            ASTVisitor visitor = new VoidPtrCastExprVisitor();
            astFunction.accept(visitor);
        }

        return super.solve();
    }

    private class VoidPtrCastExprVisitor extends ASTVisitor {

        private final INode root;

        public VoidPtrCastExprVisitor() {
            shouldVisitExpressions = true;
            root = Utils.getRoot(functionNode);
        }

        private static final String VOID_PTR_REGEX = VariableTypeUtils.VOID_TYPE.VOID + IRegex.SPACES + IRegex.POINTER;

        private boolean isOneLevelVoidPtr(String type) {
            String realType = VariableTypeUtils.getRealType(root, type);
            realType = realType.trim();
            return realType.matches(VOID_PTR_REGEX);
        }

        private boolean append(String name, String type) {
            if (!isOneLevelVoidPtr(type)) {
                type = type.replaceAll(IRegex.SPACES + IRegex.POINTER, SpecialCharacter.POINTER);
                type = VariableTypeUtils.removeRedundantKeyword(type);
                typeMap.append(name, type);
                return true;
            }

            return false;
        }

        @Override
        public int visit(IASTExpression expression) {
            if (expression instanceof IASTFunctionCallExpression) {
                String name;
                IASTExpression nameExpr = ((IASTFunctionCallExpression) expression).getFunctionNameExpression();
                if (nameExpr instanceof IASTFieldReference)
                    name = ((IASTFieldReference) nameExpr).getFieldName().getRawSignature();
                else
                    name = nameExpr.getRawSignature();
                IVariableNode parameter = getParameter(name, false);
                if (parameter != null) {
                    typeMap.putFunctionUsage(name);
                }
            } else if (expression instanceof IASTIdExpression) {
                String varName = ((IASTIdExpression) expression).getName().getRawSignature();
                IVariableNode parameter = getParameter(varName, true);
                if (parameter != null) {
                    String name = parameter.getName();
                    String type;
                    IASTNode parent = expression.getParent();
                    while (!(parent instanceof IASTStatement)) {
                        if (parent instanceof IASTCastExpression) {
                            type = ((IASTCastExpression) parent).getTypeId().getRawSignature();
                            append(name, type);
                            break;
                        } else if (parent instanceof IASTSimpleDeclaration) {
                            VariableNode var = new VariableNode();
                            var.setAST(parent);
                            type = var.getRawType();
                            append(name, type);
                            break;
                        } else if (parent instanceof IASTBinaryExpression) {
                            IASTBinaryExpression binExpr = (IASTBinaryExpression) parent;
                            if (binExpr.getOperator() == IASTBinaryExpression.op_assign
                                    && binExpr.getOperand2().equals(expression)) {
                                type = new NewTypeResolver(functionNode).solve(binExpr.getOperand1());
                                append(name, type);
                                break;
                            }
                        } else if (parent instanceof IASTFunctionCallExpression) {
                            IASTFunctionCallExpression callExpr = (IASTFunctionCallExpression) parent;
                            ICommonFunctionNode called = new MethodFinder(functionNode).find(callExpr);
                            if (called != null) {
                                IASTInitializerClause[] arguments = callExpr.getArguments();
                                int index = 0;
                                for (; index < arguments.length; index++) {
                                    if (arguments[index].equals(expression)) {
                                        break;
                                    }
                                }
                                IVariableNode argument = called.getArguments().get(index);
                                type = argument.getRealType();
                                if (!VariableTypeUtils.isVoidPointer(type)) {
                                    append(name, type);
                                    break;
                                } else {
                                    TypeMap recurTypeMap = new InternalPtrResolver(called).solve();
                                    String argumentName = argument.getName();
                                    if (typeMap.containsKey(argumentName))
                                        typeMap.append(argumentName, recurTypeMap.get(argumentName));
                                }
                            }
                            break;
                        }

                        parent = parent.getParent();
                    }
                }
            }

            return PROCESS_CONTINUE;
        }
    }
}
