package auto_testcase_generation.testdatagen.advanced_pointer;

public interface IPointerResolver {
    TypeMap solve();
}
