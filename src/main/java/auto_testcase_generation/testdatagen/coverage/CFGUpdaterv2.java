package auto_testcase_generation.testdatagen.coverage;

import auto_testcase_generation.cfg.ICFG;
import auto_testcase_generation.cfg.object.ConditionCfgNode;
import auto_testcase_generation.cfg.object.ICfgNode;
import auto_testcase_generation.cfg.testpath.FullTestpath;
import auto_testcase_generation.cfg.testpath.ITestpathInCFG;
import auto_testcase_generation.instrument.FunctionInstrumentationForStatementvsBranch_Markerv2;
import auto_testcase_generation.maker.Property_Marker;
import auto_testcase_generation.maker.StatementInTestpath_Mark;
import auto_testcase_generation.maker.TestpathString_Marker;
import util.PathUtils;
import util.VFPLogger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Update the visited state of CFG.
 *
 */
public class CFGUpdaterv2 implements ICFGUpdater {
    final static VFPLogger logger = VFPLogger.get(CFGUpdaterv2.class);
    private TestpathString_Marker testpath;
    private ICFG cfg;

    public CFGUpdaterv2(TestpathString_Marker testpath, ICFG cfg) {
        this.testpath = testpath;
        this.cfg = cfg;
    }

    @Override
    public void updateVisitedNodes() {
        // find all nodes corresponding to statements or conditions
        Set<String> visitedOffsets = getAllVisitedNodesByItsOffset(testpath);

        // update the visited state of nodes
        updateStateOfVisitedNodeByItsOffset(cfg, visitedOffsets);

        // create a chain of visited statement in order
        updateVisitedStateOfBranches(testpath, cfg);

    }

    private void updateVisitedStateOfBranches(TestpathString_Marker testpath, ICFG cfg) {
        String visitedStatementInStr = " ";
        for (String offset : testpath.getStandardTestpathByProperty(FunctionInstrumentationForStatementvsBranch_Markerv2.START_OFFSET_IN_FUNCTION))
            visitedStatementInStr += offset + " ";
//        logger.debug("visitedStatementInStr = " + visitedStatementInStr);

        int offsetOfFunctionInSourcecodeFile = cfg.getFunctionNode().getAST().getFileLocation().getNodeOffset();
        for (ICfgNode visitedNode : cfg.getVisitedStatements()) {
            if (visitedNode instanceof ConditionCfgNode) { // condition
                int tmp = (visitedNode.getAstLocation().getNodeOffset() - offsetOfFunctionInSourcecodeFile);
//                logger.debug("consider node " + tmp + " ; content  = " + ((ConditionCfgNode) visitedNode).getAst().getRawSignature());

                // analyze the true branch
                ICfgNode trueBranchNode = visitedNode.getTrueNode();
                boolean isUpdatedAsVisited = updateTheStateOfBranches(trueBranchNode, visitedNode, offsetOfFunctionInSourcecodeFile, visitedStatementInStr);
                if (isUpdatedAsVisited)
                    ((ConditionCfgNode) visitedNode).setVisitedTrueBranch(true);

                // analyze the false branch
                ICfgNode falseBranchNode = visitedNode.getFalseNode();
                isUpdatedAsVisited = updateTheStateOfBranches(falseBranchNode, visitedNode, offsetOfFunctionInSourcecodeFile, visitedStatementInStr);
                if (isUpdatedAsVisited)
                    ((ConditionCfgNode) visitedNode).setVisitedFalseBranch(true);
            }
        }
    }

    private Set<String> getAllVisitedNodesByItsOffset(TestpathString_Marker testpath) {
        Set<String> visitedOffsets = new HashSet<>();
        for (StatementInTestpath_Mark line : testpath.getStandardTestpathByAllProperties()) {
            Property_Marker startOffsetProperty = line.getPropertyByName(
                    FunctionInstrumentationForStatementvsBranch_Markerv2.START_OFFSET_IN_FUNCTION);
            Property_Marker functionProperty = line.getPropertyByName(
                    FunctionInstrumentationForStatementvsBranch_Markerv2.FUNCTION_ADDRESS);
            if (startOffsetProperty != null && functionProperty != null
                    //TODO: hỏi anh Đ.Anh về code trùng??
//                    && line.getPropertyByName(FunctionInstrumentationForStatementvsBranch_Markerv2.START_OFFSET_IN_FUNCTION) != null
            ) {
                // is statement or condition
                String relativePath = (cfg.getFunctionNode().getAbsolutePath());
                relativePath = PathUtils.toRelative(relativePath);

                if (functionProperty.getValue().equals(relativePath))
                    visitedOffsets.add(startOffsetProperty.getValue());
            }
        }
        return visitedOffsets;
    }

    private void updateStateOfVisitedNodeByItsOffset(ICFG cfg, Set<String> visitedOffsets) {
        List<ICfgNode> nodes = cfg.getAllNodes();
        for (ICfgNode node : nodes)
            if (node.getAstLocation() != null) {
                String offsetInCFG = node.getAstLocation().getNodeOffset() - cfg.getFunctionNode().getAST().getFileLocation().getNodeOffset() + "";
                if (visitedOffsets.contains(offsetInCFG)) {
                    node.setVisit(true);
                }
            }
    }

    private boolean updateTheStateOfBranches(ICfgNode branchNode, ICfgNode visitedNode,
                                             int offsetOfFunctionInSourcecodeFile, String visitedStatementInStr) {
        if (visitedNode instanceof ConditionCfgNode && branchNode != null) {
            boolean isUpdated = false;
            List<ICfgNode> flagNodes = new ArrayList<>();

            int offsetVisitedNode = visitedNode.getAstLocation().getNodeOffset();

            // ignore nodes corresponding to flag
            while (branchNode != null && branchNode.isSpecialCfgNode()) {
                flagNodes.add(branchNode);
                branchNode = branchNode.getTrueNode();  // for these type of nodes, true node and false node are the same
            }

            if (branchNode == null){
                // the current node point to the end node of cfg
                for (ICfgNode flagNode : flagNodes)
                    flagNode.setVisit(true);
                return true;

            } else {
                // make a comparison to check whether the branch node is visited
                try {
                    String comparison = " " + (offsetVisitedNode - offsetOfFunctionInSourcecodeFile) + " " +
                            (branchNode.getAstLocation().getNodeOffset() - offsetOfFunctionInSourcecodeFile) + " ";
                    if (visitedStatementInStr.contains(comparison)) {
                        isUpdated = true;
                        branchNode.setVisit(true);
//                    logger.debug("updated the branch " + comparison);
                        // update the flag nodes between the condition node and the normal nodes in its checked branch
                        for (ICfgNode flagNode : flagNodes)
                            flagNode.setVisit(true);
                        return isUpdated;
                    } else
                        return false;
                } catch (Exception e) {
                    return false;
                }

            }
        } else
            return false;
    }

    @Override
    public String[] getTestpath() {
        return new String[0];
    }

    @Override
    public void setTestpath(String[] testpath) {
        // nothing to do
    }

    @Override
    public ICFG getCfg() {
        return cfg;
    }

    @Override
    public void setCfg(ICFG cfg) {
        this.cfg = cfg;
    }

    @Override
    public ITestpathInCFG getUpdatedCFGNodes() {
        ITestpathInCFG updatedCFGNodes = new FullTestpath();
        for (ICfgNode node : getCfg().getVisitedStatements()) {
            updatedCFGNodes.getAllCfgNodes().add(node);
        }
        return updatedCFGNodes;
    }

    @Override
    public void setUpdatedCFGNodes(ITestpathInCFG updatedCFGNodes) {
        // nothing to do
    }

    @Override
    public void unrollChangesOfTheLatestPath() {
        // nothing to do
    }
}
