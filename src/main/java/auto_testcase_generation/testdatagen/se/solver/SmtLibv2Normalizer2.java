package auto_testcase_generation.testdatagen.se.solver;

import auto_testcase_generation.testdatagen.se.NewVariableInSe;
import util.Utils;
import org.eclipse.cdt.core.dom.ast.*;
import org.eclipse.cdt.core.dom.ast.cpp.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 *
 */
public class SmtLibv2Normalizer2 extends SmtLibv2Normalizer {
	private Set<NewVariableInSe> newVariableInSes;

	public SmtLibv2Normalizer2(String expression, Set<NewVariableInSe> newVariableInSes) {
		originalSourcecode = expression;
		this.newVariableInSes = newVariableInSes;
	}

	@Override
	public void normalize() {
		IASTNode ast = Utils.convertToIAST(originalSourcecode);
		normalizeSourcecode = createSmtLib(ast);

		// Ex: "(1.2)" ----------->"1.2"
		normalizeSourcecode = normalizeSourcecode.replaceAll("\\(([a-zA-Z0-9_\\.]+)\\)", "$1");
	}

	protected String createSmtLib(IASTNode ast) {
		StringBuilder normalizeSc = new StringBuilder();

		if (ast.getRawSignature().equals("NULL")) {
			normalizeSc = new StringBuilder("0");

		} else if (ast.getRawSignature().equals(NEGATIVE_ONE)) {
			normalizeSc = new StringBuilder("- 1");

		} else if (ast instanceof ICPPASTName || ast instanceof IASTIdExpression
				|| ast instanceof ICPPASTLiteralExpression || ast instanceof ICPPASTFieldReference) {
			normalizeSc = new StringBuilder(ast.getRawSignature());

		} else {
			// STEP 1. Shorten expression
			boolean isNegate = false;
			boolean isUnaryExpression = ast instanceof ICPPASTUnaryExpression;

			int count = 0;
			while (ast instanceof ICPPASTUnaryExpression) {
				if (++count > 10)
					break; // to avoid infinite loop
				ICPPASTUnaryExpression astUnary = (ICPPASTUnaryExpression) ast;
				IASTExpression astUnaryOperand = astUnary.getOperand();

				switch (astUnary.getOperator()) {
					case IASTUnaryExpression.op_plus:
						case IASTUnaryExpression.op_bracketedPrimary:
							ast = astUnaryOperand;
						break;
					case IASTUnaryExpression.op_minus:
						ast = Utils.convertToIAST(NEGATIVE_ONE + "*(" + astUnaryOperand.getRawSignature() + ")");
						break;
					case IASTUnaryExpression.op_prefixIncr:
						ast = Utils.convertToIAST("1+ " + astUnaryOperand.getRawSignature());
						break;
					case IASTUnaryExpression.op_prefixDecr:
						ast = Utils.convertToIAST(astUnaryOperand.getRawSignature() + "-1");
						break;
						case IASTUnaryExpression.op_not:
						isNegate = !isNegate;
						ast = astUnaryOperand;
						break;
					case IASTUnaryExpression.op_star: {
						ast = Utils.convertToIAST(astUnaryOperand.getRawSignature() + "[0]");
						break;
					}
					default: {
						break;
					}
				}
			}

			// STEP 2. Get operator
			String operator = "";
			if (isUnaryExpression) {
				if (isNegate) {
					operator = "not";
					normalizeSc = new StringBuilder(String.format("%s %s", operator, createSmtLib(ast)));
				} else
					normalizeSc = new StringBuilder(String.format("%s", createSmtLib(ast)));

			} else if (ast instanceof ICPPASTBinaryExpression) {
				ICPPASTBinaryExpression astBinary = (ICPPASTBinaryExpression) ast;
				switch (astBinary.getOperator()) {
				case IASTBinaryExpression.op_divide:
					operator = "div"; // integer division
					break;
				case IASTBinaryExpression.op_minus:
					operator = "-";
					break;
				case IASTBinaryExpression.op_plus:
					operator = "+";
					break;
				case IASTBinaryExpression.op_multiply:
					operator = "*";
					break;
				case IASTBinaryExpression.op_modulo:
					operator = "mod";
					break;

				case IASTBinaryExpression.op_greaterEqual:
					operator = ">=";
					break;
				case IASTBinaryExpression.op_greaterThan:
					operator = ">";
					break;
				case IASTBinaryExpression.op_lessEqual:
					operator = "<=";
					break;
				case IASTBinaryExpression.op_lessThan:
					operator = "<";
					break;
				case IASTBinaryExpression.op_equals:
					operator = "=";
					break;
				case IASTBinaryExpression.op_notequals:
					operator = "!=";
					break;

				case IASTBinaryExpression.op_logicalAnd:
					operator = "and";
					break;
				case IASTBinaryExpression.op_logicalOr:
					operator = "or";
					break;
				}

				if (operator.length() > 0)
					if (operator.equals("!="))
						if ((astBinary.getOperand1().getRawSignature().equals("NULL"))) {
							normalizeSc = new StringBuilder(String.format("(> %s 0)",
									createSmtLib(astBinary.getOperand2())));

						} else if (astBinary.getOperand2().getRawSignature().equals("NULL")) {
							normalizeSc = new StringBuilder(String.format("(> %s 0)",
									createSmtLib(astBinary.getOperand1())));

						} else
							normalizeSc = new StringBuilder(String.format("or (> %s %s) (< %s %s)",
									createSmtLib(astBinary.getOperand1()),
									createSmtLib(astBinary.getOperand2()),
									createSmtLib(astBinary.getOperand1()),
									createSmtLib(astBinary.getOperand2())));
					else
						normalizeSc = new StringBuilder(String.format("%s %s %s", operator,
								createSmtLib(astBinary.getOperand1()),
								createSmtLib(astBinary.getOperand2())));

			} else if (ast instanceof ICPPASTArraySubscriptExpression) {
				// Get all elements in array item
				List<IASTNode> elements = new ArrayList<>();

				int countWhile = 0;
				while (ast.getChildren().length > 1) {
					if (countWhile++ > 10)
						break;// to avoid infinite loop
					elements.add(0, ast.getChildren()[1]);
					ast = ast.getChildren()[0];
				}
				elements.add(ast);
				//
				IASTNode astName = elements.get(elements.size() - 1);
				normalizeSc = new StringBuilder(astName.getRawSignature());

				for (int i = elements.size() - 2; i >= 0; i--)
					normalizeSc.append(createSmtLib(elements.get(i)));
			}
		}

		normalizeSc = new StringBuilder(checkInBracket(normalizeSc.toString()) ? normalizeSc.toString() : " (" + normalizeSc + ") ");
		return normalizeSc.toString();
	}

	private boolean checkInBracket(String stm) {
		stm = stm.trim();
		if (stm.startsWith("(")) {
			int count = 0;
			for (Character c : stm.toCharArray())
				if (c == '(')
					count++;
				else if (c == ')')
					count--;
			return count == 0;
		} else
			return false;

	}

	private final String NEGATIVE_ONE = "(-1)";
}
