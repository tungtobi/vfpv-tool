package auto_testcase_generation.testdatagen.se.memory.pointer;

/**
 * Represent one level pointer
 *
 *
 */
public class PointerEnumSymbolicVariable extends PointerStructureSymbolicVariable {
    public PointerEnumSymbolicVariable(String name, String type, int scopeLevel) {
        super(name, type, scopeLevel);
    }
}
