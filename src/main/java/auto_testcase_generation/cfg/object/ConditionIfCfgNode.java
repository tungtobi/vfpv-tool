package auto_testcase_generation.cfg.object;

import org.eclipse.cdt.core.dom.ast.IASTNode;

/**
 * Example: if (a>b) {...} <br/>
 * 
 * 
 *
 */
public class ConditionIfCfgNode extends ConditionCfgNode {

	public ConditionIfCfgNode(IASTNode node) {
		super(node);
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		ConditionIfCfgNode cloneNode = (ConditionIfCfgNode) super.clone();
		cloneNode.setVisitedFalseBranch(isVisitedFalseBranch());
		cloneNode.setVisitedTrueBranch(isVisitedTrueBranch());
		return cloneNode;
	}
}
