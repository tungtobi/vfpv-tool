package thread;

/**
 * Represent a thread in VFP
 */
public class VFPThread extends Thread {

    private final AbstractVFPTask<?> task;

    public VFPThread(AbstractVFPTask task) {
        super(task);
        this.task = task;
    }

    @Override
    public void interrupt() {
        super.interrupt();
        if (task != null)
            task.cancel();
    }
}
