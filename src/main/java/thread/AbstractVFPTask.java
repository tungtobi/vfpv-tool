package thread;

import gui.BaseController;
import gui.UIController;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

/**
 * Represent a task in VFP
 * @param <V>
 */
public abstract class AbstractVFPTask<V> extends Task<V> {

    public AbstractVFPTask() {
        setOnFailed(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                BaseController.getInstance().hideLoading();
                Throwable err = getException();
                Alert alert = UIController.showDialog(Alert.AlertType.ERROR, err.getMessage(), "Error");
                err.printStackTrace();
                Platform.runLater(alert::showAndWait);
            }
        });
    }
}
