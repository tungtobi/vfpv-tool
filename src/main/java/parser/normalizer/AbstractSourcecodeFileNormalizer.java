package parser.normalizer;

/**
 * Abstract class for source code file normalization level
 *
 * 
 */
public abstract class AbstractSourcecodeFileNormalizer extends AbstractNormalizer {

    @Override
    public boolean shouldWriteToFile() {
        return true;
    }
}
