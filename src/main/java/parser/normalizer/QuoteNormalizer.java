package parser.normalizer;

import auto_testcase_generation.testdatagen.structuregen.ChangedToken;
import util.ASTUtils;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTLiteralExpression;
import parser.object.IFunctionNode;

import java.util.List;

/**
 * cout << "xxxxxxxxxxxxxxxxxxxxx" ----------------> cout << "@1"
 *
 *
 */
public class QuoteNormalizer extends AbstractFunctionNormalizer implements IFunctionNormalizer {

    public static final String PREFIX_REPLACEMENT = "@#$";
    public static int DEFAULT_ID_TOKEN = 672;
    public static int ID_TOKEN = QuoteNormalizer.DEFAULT_ID_TOKEN;

    public QuoteNormalizer() {
    }

    public QuoteNormalizer(IFunctionNode functionNode) {
        this.functionNode = functionNode;
    }

    @Override
    public void normalize() {
        QuoteNormalizer.ID_TOKEN = QuoteNormalizer.DEFAULT_ID_TOKEN;

        normalizeSourcecode = functionNode.getAST().getRawSignature();

        List<ICPPASTLiteralExpression> literalExpressions = ASTUtils.getLiteralExpressions(functionNode.getAST());

        for (ICPPASTLiteralExpression literalExpr : literalExpressions) {
            String oldContent = literalExpr.getRawSignature();
            String newContent = "\"" + QuoteNormalizer.PREFIX_REPLACEMENT + QuoteNormalizer.ID_TOKEN++ + "\"";
            normalizeSourcecode = normalizeSourcecode.replace(oldContent, newContent);

            tokens.add(new ChangedToken(oldContent, newContent));
        }
    }

}
