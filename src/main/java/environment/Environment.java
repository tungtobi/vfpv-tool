package environment;

import auto_testcase_generation.cfg.ICFG;
import boundary.BoundOfDataTypes;
import boundary.BoundaryManager;
import com.google.gson.*;
import compiler.Compiler;
import config.CommandConfig;
import config.FunctionConfig;
import config.WorkspaceConfig;
import coverage.InstructionMapping;
import parser.dependency.Dependency;
import parser.object.INode;
import parser.object.ProjectNode;
import search.Search;
import search.condition.SourcecodeFileNodeCondition;
import util.Utils;
import util.VFPLogger;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Environment {

    private final static VFPLogger logger = VFPLogger.get(Environment.class);
    
    /**
     * Singleton pattern
     */
    private static Environment instance = null;

    public synchronized static Environment getInstance() {
        if (instance == null) {
            instance = new Environment();
        }
        return instance;
    }

    private ProjectNode projectNode;
    
    private final List<Dependency> dependencies = new ArrayList<>();

    private Compiler compiler;

    private String z3Path;

    public String getZ3Path() {
        return z3Path;
    }

    private final EnviroCoverageTypeNode coverage = new EnviroCoverageTypeNode();

    private final List<EnviroDefinedVariableNode> definedVariables = new ArrayList<>();

    // save all resolved nodes to save loading cost
    private Map<String, INode> resolvedNodes = new HashMap<>();

    // save the cfg of the project
    private static final Map<String, ICFG> cfgsForBranchAndStatement = new HashMap<>();
    private static final Map<String, ICFG> cfgsForMcdc = new HashMap<>();

    private FunctionConfig defaultFunctionConfig = null;

    public Map<String, ICFG> getCfgsForMcdc() {
        return cfgsForMcdc;
    }

    public Map<String, ICFG> getCfgsForBranchAndStatement() {
        return cfgsForBranchAndStatement;
    }

    public boolean isC() {
        return getCompiler().getCompileCommand().contains("gcc");
    }

    public ProjectNode getProjectNode() {
        return projectNode;
    }

    public void setProjectNode(ProjectNode projectNode) {
        this.projectNode = projectNode;
    }

    public List<Dependency> getDependencies() {
        return dependencies;
    }

    /**
     * The coverage of the environment is changed over time. The best way is that we need to load from
     * the environment when we need.
     */
    public String getTypeofCoverage(){
        return coverage.getCoverageType(); // unspecified type of coverage
    }

    public void setTypeofCoverage(String type) {
        coverage.setCoverageType(type);
    }

    /**
     * Get all Unit Under Test physical node in project tree.
     * @return list of uut
     */
    public List<INode> getUUTs() {
        return Search.searchNodes(projectNode, new SourcecodeFileNodeCondition());
    }

    public FunctionConfig getDefaultFunctionConfig() {
        if (defaultFunctionConfig == null) {
            defaultFunctionConfig = new FunctionConfig();
        }
        return defaultFunctionConfig;
    }

    public void setCompiler(Compiler _compiler) {
        compiler = _compiler;
    }

    public Compiler getCompiler() {
        return compiler;
    }

    public static void setInstance(Environment instance) {
        Environment.instance = instance;
    }

    public static BoundOfDataTypes getBoundOfDataTypes() {
        return BoundaryManager.getInstance().getUsingBoundOfDataTypes();
    }

    public Map<String, INode> getResolvedNodes() {
        return resolvedNodes;
    }

    public void setResolvedNodes(Map<String, INode> resolvedNodes) {
        this.resolvedNodes = resolvedNodes;
    }

    public List<EnviroDefinedVariableNode> getDefinedVariables() {
        return definedVariables;
    }

    // key: path to source code file
    private InstructionMapping statementsMapping = new InstructionMapping();

    // key: path to source code file
    private InstructionMapping branchesMapping = new InstructionMapping();

    // key: path to source code file
    private InstructionMapping mcdcMapping = new InstructionMapping();

    public InstructionMapping getStatementsMapping() {
        return statementsMapping;
    }

    public void setStatementsMapping(InstructionMapping statementsMapping) {
        this.statementsMapping = statementsMapping;
    }

    public InstructionMapping getBranchesMapping() {
        return branchesMapping;
    }

    public void setBranchesMapping(InstructionMapping branchesMapping) {
        this.branchesMapping = branchesMapping;
    }

    public InstructionMapping getMcdcMapping() {
        return mcdcMapping;
    }

    public void setMcdcMapping(InstructionMapping mcdcMapping) {
        this.mcdcMapping = mcdcMapping;
    }

    public void setupConfiguration() {
        String json = Utils.readFileContent(WorkspaceConfig.CONFIG_FILE);
        JsonObject config = JsonParser.parseString(json).getAsJsonObject();

        JsonObject jsonCompiler = config.getAsJsonObject("compiler");
        compiler = new Gson().fromJson(jsonCompiler, Compiler.class);
        importDefinedVariables(jsonCompiler.getAsJsonArray("defines"));

        z3Path = config.get("z3Path").getAsString();
        setTypeofCoverage(config.get("coverage").getAsString());
    }

    private void importDefinedVariables(JsonArray jsonArray) {
        jsonArray.forEach(jsonElement -> {
            EnviroDefinedVariableNode newNode = new EnviroDefinedVariableNode();
            String string = jsonElement.getAsString().trim();
            if (!string.isEmpty()) {
                if (string.contains("=")){
                    // example: ABCD=1
                    String[] elements = string.split("=");
                    if (elements.length == 2) {
                        newNode.setName(elements[0]);
                        newNode.setValue(elements[1]);
                    }
                } else {
                    // example: ABCD
                    newNode.setName(string);
                    newNode.setValue("");
                }

                definedVariables.add(newNode);
            }
        });
    }

    private CommandConfig.LinkEntry linkEntry;

    public CommandConfig.LinkEntry getLinkEntry() {
        return linkEntry;
    }

    public void setLinkEntry(CommandConfig.LinkEntry linkEntry) {
        this.linkEntry = linkEntry;
    }
}
