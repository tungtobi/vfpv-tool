/*
 * TEST DRIVER FOR C++
 *
 * Generate automatically by VFPUTAUTO
 */

// include some necessary standard libraries
#include <cstdio>
#include <string>
#include <fstream>

#define ASSERT_ENABLE

// define maximum line of test path
#define VFP_MARK_MAX 5000

// function call counter
int VFP_fCall = 0;

// test case name
std::string VFP_test_case_name;

typedef void (*VFP_Test)();

void VFP_run_test(std::string name, VFP_Test test, int iterator);

////////////////////////////////////////
//  BEGIN TEST PATH SECTION           //
////////////////////////////////////////

#define VFP_TEST_PATH_FILE "{{INSERT_PATH_OF_TEST_PATH_HERE}}"

void VFP_append_test_path(std::string content);

int VFP_mark(std::string append);

////////////////////////////////////////
//  END TEST PATH SECTION             //
////////////////////////////////////////


////////////////////////////////////////
//  BEGIN TEST RESULT SECTION         //
////////////////////////////////////////

#define VFP_EXEC_TRACE_FILE "{{INSERT_PATH_OF_EXE_RESULT_HERE}}"

void VFP_append_test_result(std::string content);

void VFP_assert_method
(
    std::string actualName, int actualVal,
    std::string expectedName, int expectedVal,
    std::string method
);

void VFP_assert_double_method
(
    std::string actualName, double actualVal,
    std::string expectedName, double expectedVal,
    std::string method
);

void VFP_assert_ptr_method
(
    std::string actualName, void* actualVal,
    std::string expectedName, void* expectedVal,
    std::string method
);

#define NULL_STRING ""

void VFP_assert
(
    std::string actualName, int actualVal,
    std::string expectedName, int expectedVal
)
{
    VFP_assert_method
    (
        actualName, actualVal,
        expectedName, expectedVal,
        NULL_STRING
    );
}

int VFP_assert_double
(
    std::string actualName, double actualVal,
    std::string expectedName, double expectedVal
)
{
    VFP_assert_double_method
    (
        actualName, actualVal,
        expectedName, expectedVal,
        NULL_STRING
    );
}

int VFP_assert_ptr
(
    std::string actualName, void* actualVal,
    std::string expectedName, void* expectedVal
)
{
    VFP_assert_ptr_method
    (
        actualName, actualVal,
        expectedName, expectedVal,
        NULL_STRING
    );
}

////////////////////////////////////////
//  END TEST RESULT SECTION           //
////////////////////////////////////////


////////////////////////////////////////
//  BEGIN SET UP - TEAR DOWN SECTION  //
////////////////////////////////////////

/*
 * This function call before main test driver
 */
void VFP_set_up();

/*
 * This function call after main test driver
 */
void VFP_tear_down();

////////////////////////////////////////
//  END SET UP - TEAR DOWN SECTION    //
////////////////////////////////////////

// Some test cases need to include specific additional headers
/*{{INSERT_ADDITIONAL_HEADER_HERE}}*/

// Include VFPignore file
/*{{INSERT_CLONE_SOURCE_FILE_PATHS_HERE}}*/

////////////////////////////////////////
//  BEGIN TEST SCRIPTS SECTION        //
////////////////////////////////////////

// Some test cases need to include specific additional headers
/*{{INSERT_ADDITIONAL_HEADER_HERE}}*/

/*{{INSERT_TEST_SCRIPTS_HERE}}*/

////////////////////////////////////////
//  END TEST SCRIPTS SECTION          //
////////////////////////////////////////

/*
 * The main() function for setting up and running the tests.
 */
int main()
{
    VFP_set_up();

    /* Compound test case setup */

    /* add & run the tests */
/*{{ADD_TESTS_STM}}*/

    /* Compound test case teardown */

    VFP_tear_down();

    return 0;
}

////////////////////////////////////////
//  BEGIN DEFINITIONS SECTION         //
////////////////////////////////////////

void VFP_append_test_path(std::string content)
{
    static int VFP_mark_iterator = 0;

    std::ofstream outfile;
    outfile.open(VFP_TEST_PATH_FILE, std::ios_base::app);
    outfile << content;
    VFP_mark_iterator++;

    // if the test path is too long, we need to terminate the process
    if (VFP_mark_iterator >= VFP_MARK_MAX) {
        outfile << "\nThe test path is too long. Terminate the program automatically!";
        outfile.close();
        exit(0);
    }

    outfile.close();
}

void VFP_append_test_result(std::string content)
{
    std::ofstream outfile;
    outfile.open(VFP_EXEC_TRACE_FILE, std::ios_base::app);
    outfile << content;
    outfile.close();
}

int VFP_mark(std::string append)
{
    VFP_append_test_path(append + "\n");
    return 1;
}

#define VFP_BUFFER_SIZE 1024

void VFP_assert_method
(
    std::string actualName, int actualVal,
    std::string expectedName, int expectedVal,
    std::string userCode
)
{
    std::string buf = "{\n";

    buf.append("\"tag\": \"VFP function calls: ");
    char temp0[VFP_BUFFER_SIZE];
    sprintf(temp0, "%d\",", VFP_fCall);
    buf.append(temp0);
    buf.append("\n");

    if (!userCode.empty())
    {
        buf.append("\"userCode\": \"");
        buf.append(userCode);
        buf.append("\",\n");
    }

    buf.append("\"actualName\": \"");
    buf.append(actualName);
    buf.append("\",\n");
    char temp1[VFP_BUFFER_SIZE];
    sprintf(temp1, "\"actualVal\": \"%d\",", actualVal);
    buf.append(temp1);
    buf.append("\n");

    buf.append("\"expectedName\": \"");
    buf.append(expectedName);
    buf.append("\",\n");
    char temp2[VFP_BUFFER_SIZE];
    sprintf(temp2, "\"expectedVal\": \"%d\"", expectedVal);
    buf.append(temp2);
    buf.append("\n},\n");

    VFP_append_test_result(buf);
}

void VFP_assert_double_method
(
    std::string actualName, double actualVal,
    std::string expectedName, double expectedVal,
    std::string userCode
)
{
    std::string buf = "{\n";

    buf.append("\"tag\": \"VFP function calls: ");
    char temp0[VFP_BUFFER_SIZE];
    sprintf(temp0, "%d\",", VFP_fCall);
    buf.append(temp0);
    buf.append("\n");

    if (!userCode.empty())
    {
        buf.append("\"userCode\": \"");
        buf.append(userCode);
        buf.append("\",\n");
    }

    buf.append("\"actualName\": \"");
    buf.append(actualName);
    buf.append("\",\n");

    char temp1[VFP_BUFFER_SIZE];
    sprintf(temp1, "\"actualVal\": \"%lf\",", actualVal);
    buf.append(temp1);
    buf.append("\n");

    buf.append("\"expectedName\": \"");
    buf.append(expectedName);
    buf.append("\",\n");

    char temp2[VFP_BUFFER_SIZE];
    sprintf(temp2, "\"expectedVal\": \"%lf\"", expectedVal);
    buf.append(temp2);
    buf.append("\n},\n");

    VFP_append_test_result(buf);
}

void VFP_assert_ptr_method
(
    std::string actualName, void * actualVal,
    std::string expectedName, void * expectedVal,
    std::string userCode
)
{
    std::string buf = "{\n";

    buf.append("\"tag\": \"VFP function calls: ");
    char temp0[VFP_BUFFER_SIZE];
    sprintf(temp0, "%d\",", VFP_fCall);
    buf.append(temp0);
    buf.append("\n");

    if (!userCode.empty())
    {
        buf.append("\"userCode\": \"");
        buf.append(userCode);
        buf.append("\",\n");
    }

    buf.append("\"actualName\": \"");
    buf.append(actualName);
    buf.append("\",\n");

    char temp1[VFP_BUFFER_SIZE];
    sprintf(temp1, "\"actualVal\": \"%x\",", actualVal);
    buf.append(temp1);
    buf.append("\n");

    buf.append("\"expectedName\": \"");
    buf.append(expectedName);
    buf.append("\",\n");

    char temp2[VFP_BUFFER_SIZE];
    sprintf(temp2, "\"expectedVal\": \"%x\"", expectedVal);
    buf.append(temp2);
    buf.append("\n},\n");

    VFP_append_test_result(buf);
}

void VFP_run_test(std::string name, VFP_Test test, int iterator)
{
    std::string begin = "BEGIN OF " + name;
    VFP_mark(begin);

    int i;
    for (i = 0; i < iterator; i++) {
        test();
    }

    std::string end = "END OF " + name;
    VFP_mark(end);
}

void VFP_set_up()
{
    /*{{INSERT_SET_UP_HERE}}*/
}

void VFP_tear_down()
{
    /*{{INSERT_TEAR_DOWN_HERE}}*/
}

////////////////////////////////////////
//  END DEFINITIONS SECTION           //
////////////////////////////////////////