/*
 * TEST DRIVER FOR C
 *
 * Generate automatically by VFPUTAUTO
 */

// include some necessary standard libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define ASSERT_ENABLE

// define maximum line of test path
#define VFP_MARK_MAX 5000

// function call counter
int VFP_fCall = 0;

// test case name
char * VFP_test_case_name;

typedef void (*VFP_Test)();

void VFP_run_test(const char * name, VFP_Test test, int iterator);

////////////////////////////////////////
//  BEGIN TEST PATH SECTION           //
////////////////////////////////////////

#define VFP_TEST_PATH_FILE "{{INSERT_PATH_OF_TEST_PATH_HERE}}"

FILE* VFP_tp_file;

void VFP_append_test_path(char content[]);

int VFP_mark(char * append);

////////////////////////////////////////
//  END TEST PATH SECTION             //
////////////////////////////////////////


////////////////////////////////////////
//  BEGIN TEST RESULT SECTION         //
////////////////////////////////////////

#define VFP_EXEC_TRACE_FILE "{{INSERT_PATH_OF_EXE_RESULT_HERE}}"

FILE* VFP_rt_file;

void VFP_append_test_result(char content[]);

void VFP_assert_method
(
    char * actualName, int actualVal,
    char * expectedName, int expectedVal,
    char * method
);

void VFP_assert_double_method
(
    char * actualName, double actualVal,
    char * expectedName, double expectedVal,
    char * method
);

void VFP_assert_ptr_method
(
    char * actualName, void * actualVal,
    char * expectedName, void * expectedVal,
    char * method
);

void VFP_assert
(
    char * actualName, int actualVal,
    char * expectedName, int expectedVal
)
{
    VFP_assert_method
    (
        actualName, actualVal,
        expectedName, expectedVal,
        NULL
    );
}

int VFP_assert_double
(
    char * actualName, double actualVal,
    char * expectedName, double expectedVal
)
{
    VFP_assert_double_method
    (
        actualName, actualVal,
        expectedName, expectedVal,
        NULL
    );
}

int VFP_assert_ptr
(
    char * actualName, void * actualVal,
    char * expectedName, void * expectedVal
)
{
    VFP_assert_ptr_method
    (
        actualName, actualVal,
        expectedName, expectedVal,
        NULL
    );
}

////////////////////////////////////////
//  END TEST RESULT SECTION           //
////////////////////////////////////////


////////////////////////////////////////
//  BEGIN SET UP - TEAR DOWN SECTION  //
////////////////////////////////////////

/*
 * This function call before main test driver
 */
void VFP_set_up();

/*
 * This function call after main test driver
 */
void VFP_tear_down();

////////////////////////////////////////
//  END SET UP - TEAR DOWN SECTION    //
////////////////////////////////////////

// Some test cases need to include specific additional headers
/*{{INSERT_ADDITIONAL_HEADER_HERE}}*/

// Include VFPignore file
/*{{INSERT_CLONE_SOURCE_FILE_PATHS_HERE}}*/

////////////////////////////////////////
//  BEGIN TEST SCRIPTS SECTION        //
////////////////////////////////////////

// function pointer default functions
/*{{INSERT_DEFAULTS_FUNCTION_HERE}}*/

/*{{INSERT_TEST_SCRIPTS_HERE}}*/

////////////////////////////////////////
//  END TEST SCRIPTS SECTION          //
////////////////////////////////////////

/* 
 * The main() function for setting up and running the tests.
 */
int main()
{
    VFP_set_up();
    
    /* Compound test case setup */

    /* add & run the tests */
/*{{ADD_TESTS_STM}}*/

    /* Compound test case teardown */

    VFP_tear_down();
    
    return 0;
}

////////////////////////////////////////
//  BEGIN DEFINITIONS SECTION         //
////////////////////////////////////////

void VFP_append_test_path(char content[])
{
    static int VFP_mark_iterator = 0;
    
    VFP_tp_file = fopen(VFP_TEST_PATH_FILE, "a");
    fputs(content, VFP_tp_file);
    VFP_mark_iterator++;

    // if the test path is too long, we need to terminate the process
    if (VFP_mark_iterator >= VFP_MARK_MAX) {
        fputs("\nThe test path is too long. Terminate the program automatically!", VFP_tp_file);
        fclose(VFP_tp_file);
        exit(0);
    }

    fclose(VFP_tp_file);
}

void VFP_append_test_result(char content[])
{
    VFP_rt_file = fopen(VFP_EXEC_TRACE_FILE, "a");
    fputs(content, VFP_rt_file);
    fclose(VFP_rt_file);
}

#define VFP_MAX_LINE_LENGTH 100000

int VFP_mark(char * append)
{
    char build[VFP_MAX_LINE_LENGTH] = "";
    strcat(build, append);
    strcat(build, "\n");
    VFP_append_test_path(build);
    return 1;
}

#define VFP_BUFFER_SIZE 1024

void VFP_assert_method
(
    char * actualName, int actualVal,
    char * expectedName, int expectedVal,
    char * userCode
)
{
    char buf[VFP_MAX_LINE_LENGTH] = "{\n";

    strcat(buf, "\"tag\": \"VFP function calls: ");
    char temp0[VFP_BUFFER_SIZE];
    sprintf(temp0, "%d\",", VFP_fCall);
    strcat(buf, temp0);
    strcat(buf, "\n");

    if (userCode != NULL)
    {
        strcat(buf, "\"userCode\": \"");
        strcat(buf, userCode);
        strcat(buf, "\",\n");
    }

    strcat(buf, "\"actualName\": \"");
    strcat(buf, actualName);
    strcat(buf, "\",\n");
    char temp1[VFP_BUFFER_SIZE];
    sprintf(temp1, "\"actualVal\": \"%d\",", actualVal);
    strcat(buf, temp1);
    strcat(buf, "\n");

    strcat(buf, "\"expectedName\": \"");
    strcat(buf, expectedName);
    strcat(buf, "\",\n");
    char temp2[VFP_BUFFER_SIZE];
    sprintf(temp2, "\"expectedVal\": \"%d\"", expectedVal);
    strcat(buf, temp2);
    strcat(buf, "\n},\n");

    VFP_append_test_result(buf);
}

void VFP_assert_double_method
(
    char * actualName, double actualVal,
    char * expectedName, double expectedVal,
    char * userCode
)
{
    char buf[VFP_MAX_LINE_LENGTH] = "{\n";

    strcat(buf, "\"tag\": \"VFP function calls: ");
    char temp0[VFP_BUFFER_SIZE];
    sprintf(temp0, "%d\",", VFP_fCall);
    strcat(buf, temp0);
    strcat(buf, "\n");

    if (userCode != NULL) {
        strcat(buf, "\"userCode\": \"");
        strcat(buf, userCode);
        strcat(buf, "\",\n");
    }

    strcat(buf, "\"actualName\": \"");
    strcat(buf, actualName);
    strcat(buf, "\",\n");

    char temp1[VFP_BUFFER_SIZE];
    sprintf(temp1, "\"actualVal\": \"%lf\",", actualVal);
    strcat(buf, temp1);
    strcat(buf, "\n");

    strcat(buf, "\"expectedName\": \"");
    strcat(buf, expectedName);
    strcat(buf, "\",\n");

    char temp2[VFP_BUFFER_SIZE];
    sprintf(temp2, "\"expectedVal\": \"%lf\"", expectedVal);
    strcat(buf, temp2);
    strcat(buf, "\n},\n");

    VFP_append_test_result(buf);
}

void VFP_assert_ptr_method
(
    char * actualName, void * actualVal,
    char * expectedName, void * expectedVal,
    char * userCode
)
{
    char buf[VFP_MAX_LINE_LENGTH] = "{\n";

    strcat(buf, "\"tag\": \"VFP function calls: ");
    char temp0[VFP_BUFFER_SIZE];
    sprintf(temp0, "%d\",", VFP_fCall);
    strcat(buf, temp0);
    strcat(buf, "\n");

    if (userCode != NULL) {
        strcat(buf, "\"userCode\": \"");
        strcat(buf, userCode);
        strcat(buf, "\",\n");
    }

    strcat(buf, "\"actualName\": \"");
    strcat(buf, actualName);
    strcat(buf, "\",\n");

    char temp1[VFP_BUFFER_SIZE];
    sprintf(temp1, "\"actualVal\": \"%x\",", actualVal);
    strcat(buf, temp1);
    strcat(buf, "\n");

    strcat(buf, "\"expectedName\": \"");
    strcat(buf, expectedName);
    strcat(buf, "\",\n");

    char temp2[VFP_BUFFER_SIZE];
    sprintf(temp2, "\"expectedVal\": \"%x\"", expectedVal);
    strcat(buf, temp2);
    strcat(buf, "\n},\n");

    VFP_append_test_result(buf);
}

void VFP_run_test(const char * name, VFP_Test test, int iterator)
{
    char begin[VFP_BUFFER_SIZE];
    sprintf(begin, "BEGIN OF %s", name);
    VFP_mark(begin);

    int i;
    for (i = 0; i < iterator; i++) {
        test();
    }

    char end[VFP_BUFFER_SIZE];
    sprintf(end, "END OF %s", name);
    VFP_mark(end);
}

void VFP_set_up()
{
    /*{{INSERT_SET_UP_HERE}}*/
}

void VFP_tear_down()
{
    /*{{INSERT_TEAR_DOWN_HERE}}*/
}

////////////////////////////////////////
//  END DEFINITIONS SECTION           //
////////////////////////////////////////