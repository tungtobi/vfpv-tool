# VFPV: An automated test data generation tool for void & function pointers

## USER MANUAL

### Required environment
- Our implementation has been tested on the Ubuntu 18.04, Intel®Core™i5-3470 CPU @ 3.20GHz×4, 8GBs RAM memory.
- This tool is developed by Java and IntelliJ IDEA Ultimate 2020.2.
- Automated test data generation requires Z3 solver (>= 4.8.8) which is available [here](https://github.com/Z3Prover/z3).

### Overview
- This is the user manual for the tool VFPV version 1.0.0.
- You can download the tool [here](https://gitlab.com/tungtobi/vfpv-tool/-/blob/master/vfpv-tool-1.0.0.jar).
- For any information related to the source code or test data, please contact the author.

### Using this tool
After downloading, run the tool by `java -jar vfpv-tool.jar`

The main screen looks like this:
![Imgur](https://i.imgur.com/2no6t0C.png)

Open configuration file at `vfpv/config.json`

Example:
```json
{
  "compiler": {
    "compileCommand": "gcc -c",
    "linkCommand": "gcc",
    "includeFlag": "-I",
    "defineFlag": "-D",
    "outputFlag": "-o",
    "outputExtension": ".out",
    "includePaths": [],
    "defines": [
            "MY_FLAG=1"
        ]
  },
  "coverage": "STATEMENT",
  "z3Path": "z3-4.8.8-x64-osx-10.14.6/bin/z3"
}
```

where `z3Path` points to z3 executable file path.

To create a new testing environment, please `Browser` the project and click `Load` to build the environment.

In order to generate test data, at first, choose a file. Then, choose a function and click `Generate`.

After finish processing, generated test data is shown in screen, you can double click to view values of test data or click `Highlight` to view covered statements.
